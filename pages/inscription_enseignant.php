<?php
include('../scripts/function.php');
include('../scripts/db.php');
enTete("Ajouter un enseignant");
nav("Tableau de bord");
headerBig("Ajouter un enseignant");
$db = initDb();
// Validation du formulaire
if (isset($_POST['submit'])) {
  $err_msg="";
	if (isset($_POST['identifiant'])){
    $identifiant = htmlspecialchars($_POST['identifiant']);
    $identifiants = getAllIdentifiants($db);
    foreach ($identifiants as $id) {
      if($_POST['identifiant'] == $id[0]){
        $err_msg.="Identifiant déjà utilisé<br />";
        $identifiant = null;
      }
    }
  }else{
    $err_msg .= "Veuillez saisir un identifiant<br />";
  }
  if (isset($_POST['mot_de_passe']) && ($_POST['mot_de_passe'] == $_POST['verif_mdp'])){
    $mot_de_passe = password_hash(htmlspecialchars($_POST['mot_de_passe']),PASSWORD_BCRYPT);
  }else{
    $err_msg .= "Veuillez saisir un mot de passe et le valider<br />";
  }
  if (isset($_POST['nom'])){
    $nom = htmlspecialchars($_POST['nom']);
  }else{
    $err_msg .= "Veuillez saisir un nom<br />";
  }
  if (isset($_POST['prenom'])){
    $prenom = htmlspecialchars($_POST['prenom']);
  }else{
    $err_msg .= "Veuillez saisir un prénom<br />";
  }
  if($nom != null && $prenom!= null && $identifiant!= null && $mot_de_passe!=null) {
    addPersonne($db,array($nom,$prenom,NULL,$identifiant,$mot_de_passe,'enseignant'));
    $personne = getPersonneByIdentifiant($db,$identifiant);
    addEnseignant($db,$personne['id_personne']);
    header('Location: https://moduleweb.esigelec.fr/grp_9_5/pages/tableau_de_bord.php');
    exit;
  }
}
echo"
<section class='m-2'>
  <div class='container'>
    <div class='row d-flex justify-content-center align-items-center'>
      <div class='col-lg-12 col-xl-11'>
        <div class='card text-black' style='border-radius: 25px;'>
          <div class='card-body p-md-5'>
            <div class='row justify-content-center'>
              <div class='col-md-12 col-lg-10 col-xl-8 order-2 order-lg-1'>
                <p class='text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4'>Inscription Enseignant</p>";
                if(isset($err_msg)){
                  echo "<p class='text-danger'>".$err_msg."</p>";
                }
                echo"<form method='post' class='mx-1 mx-md-4'>
                  <div class='d-flex flex-row align-items-center mb-4'>
                    <i class='fas fa-user fa-lg me-3 fa-fw'></i>
                    <div class='form-outline flex-fill m-2'>
                      <input type='text' class='form-control' name='nom' />
                      <label class='form-label' for='form3Example1c'>Nom</label>
                    </div>
                    <div class='form-outline flex-fill m-2'>
                      <input type='text' class='form-control' name='prenom' />
                      <label class='form-label' for='form3Example1c'>Prénom</label>
                    </div>
                  </div>
                  <div class='d-flex flex-row align-items-center mb-4'>
                  <i class='fas fa-lock fa-lg me-3 fa-fw'></i>
                    <div class='form-outline flex-fill mb-0'>
                      <input type='text' id='form3Example3c' class='form-control'name='identifiant' />
                      <label class='form-label' for='form3Example3c'>Identifiant</label>
                    </div>
                  </div>

                  <div class='d-flex flex-row align-items-center mb-4'>
                    <i class='fas fa-lock fa-lg me-3 fa-fw'></i>
                    <div class='form-outline flex-fill mb-0'>
                      <input type='password' id='form3Example4c' class='form-control' name='mot_de_passe' />
                      <label class='form-label' for='form3Example4c'>Mot de passe</label>
                    </div>
                  </div>

                  <div class='d-flex flex-row align-items-center mb-4'>
                    <i class='fas fa-key fa-lg me-3 fa-fw'></i>
                    <div class='form-outline flex-fill mb-0'>
                      <input type='password' id='form3Example4cd' class='form-control' name='verif_mdp' />
                      <label class='form-label' for='form3Example4cd'>Retaper le mot   de passe</label>
                    </div>
                  </div>
                  <div class='d-flex justify-content-center mx-4 mb-3 mb-lg-4'>
                    <button type='submit' class='btn btn-primary btn-lg' name='submit'>Inscrire</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>";
    pied();
?>