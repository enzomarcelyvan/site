<?php
  include('../scripts/db.php');
  include('../scripts/function.php');

  enTete("Home");
  nav("Home");
  headerBig("Votre compte");

if (!isset($_SESSION['identifiant'])) {
  header('Location: ../index.php');
  exit;
}

$db = initDb();
$personne = getPersonneByIdentifiant($db,$_SESSION['identifiant']);
if($personne['fonction']=='etudiant'){
  $etudiant = getEtudiant($db,$personne['id_personne']);
  if(isset($_GET['inscription'])){
    if($etudiant['sujet'] == null){
      inscrireEtudiant($db,$_GET['inscription'],$personne['id_personne']);
    }else{
      echo"Vous êtes déjà inscrit à un sujet";
    }
  }
  if(isset($_GET['desinscription'])){
    delSujetFromEtudiant($db,$etudiant['id_etudiant'],$_GET['desinscription']);
  }
}

echo"
<section class='mb-3 mt-3'>
<div class='container'>
  <div class='row'>
    <div class='col-6'>
      <div class='card p-3'>
        <div class='d-flex align-items-center'>
          <div class='ml-3 w-100'>
            <h4 class='mb-0 mt-0'>" . $personne['prenom'] . " " . $personne['nom'] . "</h4> 
            <span> Fonction : " . $personne['fonction'] . "</span> <br />
            <span> Date de naissance : " . $personne['date_naissance'] . "</span>
          </div>
        </div>
      </div>
    </div>";
if($personne['fonction'] == "etudiant"){
  echo"
  <div class='col-6'>
    <div class='card p-3'>
        <div class='align-items-center'>
            <div class='ml-3 w-100'>
              <h4 class='mb-0 mt-0'>Votre choix</h4>";
              if(getSujetEtudiant($db,$personne['id_personne'])['sujet'] != null){
                $sujet = getSujet($db,getSujetEtudiant($db,$personne['id_personne'])[0]);
                echo "<p>".$sujet['nom_sujet']."</p>";
                echo"<a href='sujet_detaille.php?sujet=".$sujet['id_sujet']."'>Page détaillé</a>";
              }else{
                echo"<p> Vous n'avez pas encore choisis de sujet. Aller dans Liste sujets pour en choisir un.</p>";
              }
            echo"</div>
          </div>
        </div>
      </div>
    </div>
  </div>";
}else if($personne['fonction'] == "enseignant"){
  $enseignant = getEnseignant($db,$personne['id_personne']);

  if($enseignant['mon_sujet'] == null){
    echo"<div class='col-6'>
    <div class='card p-3'>
        <div class='align-items-center'>
            <div class='ml-3 w-100'>
              <h4 class='mb-0 mt-0'>Vous n'avez pas encore créer de sujet. Aller dans la page mon sujet pour en créer un.</h4>
          </div>
        </div>
      </div>
    </div>
  </div>";
  }else{
    $sujet = getSujet($db,$enseignant['mon_sujet']);
    $dominante = getDominante($db,$sujet['dominante']);
    $departement = getDepartement($db,$sujet['departement']);
    echo"<div class='col-6'>
          <div class='card p-3'>
            <div class='align-items-center'>
              <div class='ml-3 w-100'>
                <h4 class='mb-0 mt-0'>Votre sujet</h4>
                <div class='card-body'>
                <h5 class='card-title'>Nom du sujet : ".$sujet['nom_sujet']."</h5>
                <ul class='list-group list-group-flush'>
                    <li class='list-group-item'>Département : ".$departement['nom_departement']."</li>
                    <li class='list-group-item'>Dominante : ".$dominante['nom_dominante']."</li>
                    <li class='list-group-item'>Enseignant : ".$personne['nom']."</li>
                    <li class='list-group-item'>Nombres de places restantes : ".$sujet['nb_places_rest']."</li>
                </ul>
                <p class='card-text'>".$sujet['resume_sujet']."</p>";
              if($sujet['chemin_piece_jointe'] != null){
                  echo"<a href='../bdd/pdfsujet/".$sujet['chemin_piece_jointe']."' download='".$sujet['chemin_piece_jointe']."'>Télécharger le pdf</a>";
              }
          echo"</div>
          <div class='table-responsive'>
                    <table class='table table-striped table-bordered table-hover'>
                    <caption>Liste des étudiants inscrits</caption>
                    <thead>
                        <tr>
                            <th class='sortable-numeric' scope='col'>Nombres lignes</th>
                            <th scope='col'>Nom</th>
                            <th scope='col'>Prénom</th>
                        </tr>
                    </thead>
                    <tbody>";
                    $etudiants = getAllEtudiantInscrit($db,$sujet['id_sujet']);
                    $cpt = 1;
                    foreach($etudiants as $etudiant){
                      $personneInscrit = getPersonne($db,$etudiant['id_etudiant']);
                      echo "<tr>
                      <th scope='row'>$cpt</th>
                      <td>" . $personneInscrit['nom'] . "</td>
                      <td>" . $personneInscrit['prenom'] . "</td>";
                      $cpt++;
                    }
                    echo"</tbody>
                    </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>";
  }
}


echo"
</section>";
pied();
?>
