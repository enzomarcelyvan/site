<?php
    include('../scripts/function.php');
    include('../scripts/db.php');
    enTete('Mon sujet');
    nav('Mon sujet');
    headerBig("Modifier sujet");
    $db=initDb();
    $enseignant=getEnseignantFromId($db,$_SESSION['identifiant']);
    $sujet=getSujet($db,$enseignant['mon_sujet']);
    $dominanteSujet=$sujet['dominante'];
    $departement=$sujet['departement'];
    $placesRestantes=$sujet['nb_places_rest'];
    $resume=$sujet['resume_sujet'];
    $nomSujet=htmlspecialchars($sujet['nom_sujet'], ENT_QUOTES);
    $fichier=$sujet['chemin_piece_jointe'];
    if (isset($_POST['submit'])) {
        $err_msg="";
        if (isset($_POST['departement'])){
          $departement = htmlspecialchars($_POST['departement']);
        }else{
          $err_msg .= "<span style='color: red;'>Veuillez choisir un département</span><br />";
        }
        if (isset($_POST['dominante'])){
          $dominante = htmlspecialchars($_POST['dominante']);
        }else{
          $err_msg .= "Veuillez choisir une dominante<br/>";
        }
        if (isset($_POST['nom']) && $_POST['nom']!=""){
          $titreSujet = htmlspecialchars($_POST['nom']);
        }else{
          $err_msg .= "Veuillez saisir un nom de sujet<br/>";
        }
        if(isset($_POST['sujet'])){
            $fichier=$_POST['sujet'];
        }else{
            $fichier=$_FILES['sujet']['name'];
        }
        if(isset($_POST['nbPlaces']) && $_POST['nbPlaces']!=""){
          $nbPlaces=$_POST['nbPlaces'];
        }else{
          $err_msg .= "Veuillez indiquer le nombre de places<br>";
        }
        if(isset($_POST['description'])){
          $description=$_POST['description'];
        }else{
          $err_msg .= "Veuillez donner une description au sujet<br>";
        }

        if($err_msg=="" && $titreSujet != null && $nbPlaces!="" && $dominante!= null && $departement!= null && $nbPlaces!=null && $nbPlaces!="" && $description!=null) {
          if($departement=="TIC")
              $departement=1;
          else if($departement=="SEI")
              $departement=2;
          else if($departement=="GEE")
              $departement=3;
          else if($departement=="ET")
              $departement=4;
      
          switch($dominante){
              case "CERT":
                  $dominante=1;
                  break;
              case "BDTN":
                  $dominante=2;
                  break;
              case "IA-IR":
                  $dominante=3;
                  break;
              case "ISN":
                  $dominante=4;
                  break;
              case "IF":
                  $dominante=5;
                  break;
              case "ARI":
                  $dominante=6;
                  break;
              case "EDD":
                  $dominante=7;
                  break;
              case "GET":
                  $dominante=8;
                  break;
              case "IA-DES":
                  $dominante=9;
                  break;
              case "MCTGE":
                  $dominante=10;
                  break;
              case "ISE-VA":
                  $dominante=11;
                  break;
              case "ISE-OC":
                  $dominante=12;
                  break;
              case "ISYMED":
                  $dominante=13;
                  break;
              case "ESAA":
                  $dominante=14;
                  break;
              case "ICOM":
                  $dominante=15;
                  break;
              default:
                  break;
          }
            updateSujet($db, $departement, $dominante, $titreSujet, $description, $nbPlaces, $fichier, $sujet['id_sujet']);
            $uploaddir = '../bdd/pdfsujet/';
            $uploadfile = $uploaddir . basename($_FILES['sujet']['name']);
            if (move_uploaded_file($_FILES['sujet']['tmp_name'], $uploadfile)) {
            echo "Le fichier est valide, et a été téléchargé
                    avec succès.";
            } else {
            echo "Attaque potentielle par téléchargement de fichiers.
                    Voici plus d'informations :\n";
            }
          header('Location: home.php');
        }
  }
  echo"
  <section class='m-2'>
    <div class='container'>
      <div class='row d-flex justify-content-center align-items-center'>
        <div class='col-lg-12 col-xl-11'>
          <div class='card text-black' style='border-radius: 25px;'>
            <div class='card-body p-md-5'>
              <div class='row justify-content-center'>
                <div class='col-md-12 col-lg-10 col-xl-8 order-2 order-lg-1'>";
                  if(isset($err_msg)){
                    echo "<p class='text-danger'>".$err_msg."</p>";
                  }
                  echo"<form method='post' class='mx-1 mx-md-4' enctype='multipart/form-data' action='modif_sujet.php'>
                    <div class='d-flex flex-row align-items-center mb-4'>
                      <i class='fas fa-user fa-lg me-3 fa-fw'></i>
                      <div class='form-outline flex-fill m-2'>
                        <input  id = 'title' type='text' class='form-control' name='nom' value='".$nomSujet."'>
                        <label class='form-label' for='title'>Nom du sujet</label>
                      </div>
                      <div class='form-outline flex-fill m-2'>
                        <input id = 'remaining' type='text' class='form-control' name='nbPlaces' value='".$placesRestantes."'>
                        <label class='form-label' for='remaining'>Nombre de places</label>
                      </div>
                    </div>
                    <div class='d-flex flex-row align-items-center mb-4'>
                      <i class='fas fa-exclamation fa-lg me-4 fa-fw'></i>
                      <div class='col-md-5 m-2'>
                          <select  class='form-select' name='dominante' id= 'domin'>
                          <option disabled value> -- selectionner -- </option>";
                          $dominantes= getAllDominantes($db);
                          $cpt =1;
                          $nomDominante = getDominante($db, $dominanteSujet);
                          foreach ($dominantes as $dominante) {
                            if($dominante['nom_dominante']==$nomDominante['nom_dominante']){
                              echo"<option selected  value='".$dominante['nom_dominante']."'>".$dominante['nom_dominante']."</option>";
                              $cpt++;
                            }else{
                              echo"<option value='".$dominante['nom_dominante']."'>".$dominante['nom_dominante']."</option>";
                              $cpt++;
                            }
                          }
                          echo"</select>
                          <label class='form-label' for='domin'>Dominante</label>
                      </div>
                      <div class='col-md-5 m-2'>
                          <select  id = 'listDepartement' class='form-select' name='departement'>
                          <option disabled value> -- selectionner -- </option>";
                          $departements= getAllDepartements($db);
                          $cpt =1;
                          $nomDepartement=getDepartement($db, $departement);
                          foreach ($departements as $departement) {
                            if($departement['nom_departement']==$nomDepartement['nom_departement']){
                              echo"<option selected value='".$departement['nom_departement']."'>".$departement['nom_departement']."</option>";
                              $cpt++;
                            }else{
                              echo"<option value='".$departement['nom_departement']."'>".$departement['nom_departement']."</option>";
                              $cpt++;
                            }
                          }
                          echo"</select>
                          <label class='form-label' for='listDepartement'>Département</label>
                      </div>
                    </div>";

                    $pdfdir = "../bdd/pdfsujet/";
                    $pdfdir.=$fichier;
                    if(!strchr($pdfdir,".pdf")){
                        $pdfdir.=".pdf";
                    }

                    echo"
                    <div>
                        <object  style = 'border-radius:20px; min-width:100%'  id= 'apercu' data='".$pdfdir."' height='480'></object>
                    </div>
                    <div class='form-group'>
                          <div>
                            <label id = 'label' for='modifier'>".$sujet['chemin_piece_jointe']."</label>
                            <input type='button' id= 'modifier' name = 'modifier' value='Modifier' onclick='parcourir()'/>
                            <script>
                            function parcourir(){
                              document.getElementById('hiddenFile').click();
                              document.getElementById('modifier').style.visibility="."'hidden'"."
                              document.getElementById('hiddenFile').style.visibility="."'visible'"."
                              document.getElementById('label').style.visibility="."'hidden'"."
                            }
                          </script>
                            <input type='hidden' name='MAX_FILE_SIZE' value='100000000' />
                            <input type='file' class='form-control-file' id='hiddenFile' name='sujet' style='visibility:hidden'>
                          </div>
                    </div>
                    <br>
                    <div class='form-control'>
                      <label for='exampleFormControlTextarea1'>Description</label>
                      <textarea class='form-control' id='exampleFormControlTextarea1' rows='3' name='description'>$resume</textarea>
                    </div>
                    <br>
                    <div class='d-flex justify-content-center mx-4 mb-3 mb-lg-4'>
                      <button type='submit' class='btn btn-primary btn-lg' name='submit'>Déposer</button>

                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>";
?>
<?php
    pied();
?>
