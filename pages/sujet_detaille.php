<?php
include('../scripts/function.php');
include('../scripts/db.php');
enTete("Sujet détaillé");
nav("Sujet détaillé");
headerBig("Sujet détaillé");
$db = initDb();
if(isset($_GET['sujet'])){
    $sujet = getSujet($db,$_GET['sujet']);
    $dominante = getDominante($db,$sujet['dominante']);
    $departement = getDepartement($db,$sujet['departement']);
    $enseignant = getEnseignantBySujet($db,$sujet['id_sujet']);
    $personne = getPersonne($db,$enseignant['id_enseignant']);
    
    if($_SESSION['fonction']=='etudiant'){
        $etudiant = getEtudiant($db,getPersonneByIdentifiant($db,$_SESSION['identifiant'])['id_personne']);
        echo "<section class='p-3'>
        <div class='row'>
            <div class='col-2'></div>
            <div class='col-8'>
                <div class='card'>";

                if($sujet['chemin_piece_jointe'] != null && $sujet['chemin_piece_jointe'] != ""){
                    $pdfdir = "../bdd/pdfsujet/".$sujet['chemin_piece_jointe'];
                    echo"
                        <object id= 'apercu' data='".$pdfdir."' style='min-width:100%' height='480'></object>
                    ";
                }else{
                    echo"<img src='../dist/assets/img/listeSujets/".$dominante['nom_dominante'].".jpg' class='card-img-top' alt='image de la dominante'>";
                }

                echo"
                    <div class='card-body'>
                        <h5 class='card-title'>Nom du sujet : ".$sujet['nom_sujet']."</h5>
                        <ul class='list-group list-group-flush'>
                            <li class='list-group-item'>Département : ".$departement['nom_departement']."</li>
                            <li class='list-group-item'>Dominante : ".$dominante['nom_dominante']."</li>
                            <li class='list-group-item'>Enseignant : ".$personne['nom']."</li>
                            <li class='list-group-item'>Nombres de places restantes : ".$sujet['nb_places_rest']."</li>
                        </ul>
                        <p class='card-text'>".$sujet['resume_sujet']."</p>";
                        if($etudiant['sujet'] == null)
                            if($sujet['nb_places_rest'] > 0){
                                echo"<a href='home.php?inscription=".$sujet['id_sujet']."' class='btn btn-primary' id='inscription'>S'inscrire</a>";
                            }else
                                echo "<p>Plus de places dans ce sujet !</p>";
                        else if($etudiant['sujet'] == $sujet['id_sujet']){
                            echo"<a href='home.php?desinscription=".$sujet['id_sujet']."' class='btn btn-primary' id='inscription'>Se désinscrire</a>";
                        }else{
                            echo"<p>Désincrivez-vous de votre ancien sujet pour s'inscrire à celui-ci </p>";
                        }
                        if($sujet['chemin_piece_jointe'] != null){
                        echo"<a href='../bdd/pdfsujet/".$sujet['chemin_piece_jointe']."' download='".$sujet['chemin_piece_jointe']."'>Télécharger le pdf</a>";
                        }
                    echo"</div>
                </div>
            </div>
            <div class='col-2'></div>
        </div>
    </section>";
    }else{
        echo "
        <section class='p-3'>
        <div class='row'>
            <div class='col-2'></div>
            <div class='col-8'>
                <div class='card'>";
                if($sujet['chemin_piece_jointe'] != null && $sujet['chemin_piece_jointe'] != ""){
                    $pdfdir = "../bdd/pdfsujet/".$sujet['chemin_piece_jointe'];
                    echo"
                        <object  id= 'apercu' data='".$pdfdir."' style='min-width:100%' height='480'></object>
                    ";
                }else{
                    echo"<img src='../dist/assets/img/listeSujets/".$dominante['nom_dominante'].".jpg' class='card-img-top' alt='image de la dominante'>";
                }
                echo"
                    <div class='card-body'>
                        <h5 class='card-title'>Nom du sujet :".$sujet['nom_sujet']."</h5>
                        <ul class='list-group list-group-flush'>
                            <li class='list-group-item'>Département : ".$departement['nom_departement']."</li>
                            <li class='list-group-item'>Dominante : ".$dominante['nom_dominante']."</li>
                            <li class='list-group-item'>Enseignant : ".$personne['nom']."</li>
                            <li class='list-group-item'>Nombres de places restantes : ".$sujet['nb_places_rest']."</li>
                        </ul>
                        <p class='card-text'>".$sujet['resume_sujet']."</p>";
                        if($sujet['chemin_piece_jointe'] != null){
                            echo"<a href='../bdd/pdfsujet/".$sujet['chemin_piece_jointe']."' download='".$sujet['chemin_piece_jointe']."'>Télécharger le pdf</a>";
                        }
                    echo"</div>
                </div>
            </div>
            <div class='col-2'></div>
        </div>
    </section>";
    }
}else{
    header('Location: home.php');
    exit;
}
pied();
?>
