<?php
include('../scripts/function.php');
include('../scripts/db.php');
$db = initDb();
enTete("Connexion");
nav("Connexion");

// Validation du formulaire
if (isset($_POST['submit'])) {
	if (isset($_POST['identifiant']))
		$identifiant = htmlspecialchars($_POST['identifiant']);
	if (isset($_POST['mot_de_passe']))
		$mot_de_passe = htmlspecialchars($_POST['mot_de_passe']);
		
	$personne = getPersonneByIdentifiant($db, $identifiant);
	if ($identifiant == $personne['identifiant']) {
		if (password_verify($mot_de_passe,$personne['mot_de_passe'])) {
			$_SESSION['identifiant'] = $identifiant;
			$_SESSION['fonction'] = $personne['fonction'];
			//En fonction de la fonction de la personne on la redirige vers sa page 'préféré'
			switch ($_SESSION['fonction']) {
				case 'administrateur':
					header('Location: tableau_de_bord.php');
					exit;
					break;
				case 'enseignant':
					header('Location: mon_sujet.php');
					exit;
					break;
				case 'etudiant':
					header('Location: liste_sujet.php');
					exit;
					break;
				default:
					header('Location: home.php');
					exit;
					break;
			}
		} else {
			header('Location: connexion.php?error=motdepasse');
			exit;
		}
	} else {
		header('Location: connexion.php?error=identifiant');
		exit;
	}
	header('Location: connexion.php?error=username');
	exit;
}
echo"
<header class='masthead bg-primary text-white text-center'>
	<div class='container d-flex align-items-center flex-column'>
		<h1 class='masthead-heading text-uppercase mb-0'>Connexion</h1>
	</div>
</header>
<section class='page-section Formulaire' id='Formulaire'>
	<div class='container'>
		<h2 class='text-center'>Entrez vos identifants ou inscrivez vous.</h2>
		<div class='row'>
			<div class='col-3 col-md-4'></div>
			<div class='col-6 col-md-4'>";
				 if (!isset($_SESSION['identifiant'])) : 
					echo"<form method='post'>
						<label for='identifiant' class='form-label'>Identifiant</label>
						<input type='text' class='form-control' id='identifiant' name='identifiant' aria-describedby='identifiant-help' placeholder='identifiant'>
						<div id='identifiant-help' class='form-text'>L'identifiant utilisé lors de la création de compte.</div>
						<label class='form-label'>Mot de passe</label>
						<input type='password' class='form-control' id='mot_de_passe' name='mot_de_passe'>
						<div class='checkbox mb-3'>
							<label>
								<input type='checkbox' value='remember-me'> Se souvenir
							</label>
						</div>
						<button class='btn btn-lg btn-primary btn-block' type='submit' name='submit'>Se connecter</button>
					</form>";
				else :
					header('Location: home.php');
					exit;
				endif;
			echo"</div>
			<div class='col-3 col-md-4'> ";
			if(isset($_GET['error'])){
				if($_GET['error'] == 'identifiant' )
					echo "Mauvais identifiant";
					else if($_GET['error'] == 'motdepasse' )
					echo "Erreur de mot de passe";
			}
			echo"</div>
		</div>
	</div>
</section>";
pied(); 
?>