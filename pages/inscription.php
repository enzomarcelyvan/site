<?php
include('../scripts/function.php');
include('../scripts/db.php');
enTete('Inscription');
nav('Inscription');
headerBig('Inscription');
$db = initDb();
$departement = $dominante = $identifiant = $mot_de_passe = $nom = $prenom = $date ="";
// Validation du formulaire
if (isset($_POST['submit'])) {
  $err_msg="";
	if (isset($_POST['identifiant']) && $_POST['identifiant'] != ""){
    $identifiant = htmlspecialchars($_POST['identifiant']);
    $identifiants = getAllIdentifiants($db);
    foreach ($identifiants as $id) {
      if($_POST['identifiant'] == $id[0]){
        $err_msg.="Identifiant déjà utilisé.<br />";
        $identifiant = null;
      }
    }
  }else{
    $err_msg .= "Veuillez saisir un identifiant<br />";
  }
  if (isset($_POST['mot_de_passe']) && ($_POST['mot_de_passe'] == $_POST['verif_mdp'])){
    $mot_de_passe = password_hash(htmlspecialchars($_POST['mot_de_passe']),PASSWORD_BCRYPT);
  }else{
    $err_msg .= "Veuillez saisir un mot de passe et le valider.<br />";
  }
  if (isset($_POST['nom']) && $_POST['nom'] != "" ){
    $nom = htmlspecialchars($_POST['nom']);
  }else{
    $err_msg .="Veuillez saisir un nom.<br />";
  }
  if (isset($_POST['prenom']) && $_POST['prenom'] != ""){
    $prenom = htmlspecialchars($_POST['prenom']);
  }else{
    $err_msg .= "Veuillez saisir un prénom.<br />";
  }
  if (isset($_POST['anniversaire']) && $_POST['anniversaire'] != ""){
    $date = htmlspecialchars($_POST['anniversaire']);
  }else{
    $err_msg .= "Veuillez saisir votre date de naissance.<br />";
  }
  if (isset($_POST['dominante'])){
    $dominante   = htmlspecialchars($_POST['dominante']);
    $departement = getDepartement($db,getDominanteByName($db,$dominante)['departement']);
  }else if(isset($_POST['departement']) ){
    $departement = getDepartementByName($db,htmlspecialchars($_POST['departement']));
    $dominante = "";
  }else{
    $err_msg .= "Veuillez choisir une dominante ou un département.<br />";
  }
  if($nom != "" && $prenom!= "" && $identifiant!= "" && $mot_de_passe!="" && $date!="" && ($departement != "" && $dominante != "") ) {
    addPersonne($db,array($nom,$prenom,$date,$identifiant,$mot_de_passe,'etudiant'));
    $personne = getPersonneByIdentifiant($db,$identifiant);
    addEtudiant($db,array($personne['id_personne'],NULL,$departement['id_departement']));
    header('Location: https://moduleweb.esigelec.fr/grp_9_5/pages/connexion.php');
    exit;
  }
}
echo"
<section class='m-2'>
  <div class='container'>
    <div class='row d-flex justify-content-center align-items-center'>
      <div class='col-lg-12 col-xl-11'>
        <div class='card text-black' style='border-radius: 25px;'>
          <div class='card-body p-md-5'>
            <div class='row justify-content-center'>
              <div class='col-md-12 col-lg-10 col-xl-8 order-2 order-lg-1'>
                <h2 class='text-center h1 fw-bold mb-5 mx-1 mx-md-4 mt-4'>Inscription</h2>";
                if(isset($err_msg)){
                  echo "<p class='text-danger'>".$err_msg."</p>";
                }
                echo"<form method='post' class='mx-1 mx-md-4'>
                  <div class='d-flex flex-row align-items-center mb-4'>
                    <i class='fas fa-user fa-lg me-3 fa-fw'></i>
                    <div class='form-outline flex-fill m-2'>
                      <input type='text' class='form-control' name='nom' />
                      <label class='form-label'>Nom</label>
                    </div>
                    <div class='form-outline flex-fill m-2'>
                      <input type='text' class='form-control' name='prenom' />
                      <label class='form-label'>Prénom</label>
                    </div>
                    <div class='form-outline flex-fill m-2'>
                      <input type='date' class='form-control' name='anniversaire' />
                      <label class='form-label' for='form3Example1c'>Date de naissance</label>
                    </div>
                  </div>
                  <div class='d-flex flex-row align-items-center mb-4'>
                    <i class='fas fa-exclamation fa-lg me-4 fa-fw'></i>
                    <div class='col-md-5 m-2'>
                        <select class='form-select' name='dominante'>
                        <option disabled selected value> -- selectionner -- </option>";
                        $dominantes= getAllDominantes($db);
                        $cpt =1;
                        foreach ($dominantes as $dominante) {
                          echo"<option value='".$dominante['nom_dominante']."'>".$dominante['nom_dominante']."</option>";
                          $cpt++;
                        }
                        echo"</select>
                        <label class='form-label'>Dominante</label>
                    </div>
                    <div class='col-md-5 m-2'>
                        <select class='form-select' name='departement'>
                        <option disabled selected value> -- selectionner -- </option>";
                        $departements= getAllDepartements($db);
                        $cpt =1;
                        foreach ($departements as $departement) {
                          echo"<option value='".$departement['nom_departement']."'>".$departement['nom_departement']."</option>";
                          $cpt++;
                        }
                        echo"</select>
                        <label class='form-label'>Département</label>
                    </div>
                  </div>
                  <div class='d-flex flex-row align-items-center mb-4'>
                  <i class='fas fa-lock fa-lg me-3 fa-fw'></i>
                    <div class='form-outline flex-fill mb-0'>
                      <input type='text' id='form3Example3c' class='form-control' name='identifiant' />
                      <label class='form-label' for='form3Example3c'>Identifiant</label>
                    </div>
                  </div>

                  <div class='d-flex flex-row align-items-center mb-4'>
                    <i class='fas fa-lock fa-lg me-3 fa-fw'></i>
                    <div class='form-outline flex-fill mb-0'>
                      <input type='password' class='form-control' name='mot_de_passe' />
                      <label class='form-label' >Mot de passe</label>
                    </div>
                  </div>

                  <div class='d-flex flex-row align-items-center mb-4'>
                    <i class='fas fa-key fa-lg me-3 fa-fw'></i>
                    <div class='form-outline flex-fill mb-0'>
                      <input type='password' class='form-control' name='verif_mdp' />
                      <label class='form-label'>Retaper votre mot de passe</label>
                    </div>
                  </div>
                  <div class='d-flex justify-content-center mx-4 mb-3 mb-lg-4'>
                    <button type='submit' class='btn btn-primary btn-lg' name='submit'>S'inscrire</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>";
pied();
?>
