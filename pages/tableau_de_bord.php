<?php
include('../scripts/function.php');
include('../scripts/db.php');
enTete("Tableau de bord");

if ($_SESSION['fonction'] != 'administrateur') {
    header('Location: https://moduleweb.esigelec.fr/grp_9_5/');
    exit;
}

$db = initDb();
// Affichage dynamique de la case selectionné
$active = array('', '', '', '','');
if (isset($_GET['menu'])) {
    switch ($_GET['menu']) {
        case 'personnes':
            $active = array('active', '', '', '','');
            if (isset($_GET['supprimer'])){
                delPersonne($db, $_GET['supprimer']);
            }
            if(isset($_POST['modifPersonne'])){
                updatePersonne($db,$_POST['fonction'],$_POST['modifPersonne']);
            }
            break;
        case 'sujets':
            $active = array('', 'active', '', '','');
            if (isset($_GET['supprimer']))
                delSujet($db, $_GET['supprimer']);
            break;
        case 'departements':
            $active = array('', '', 'active', '','','');
            if (isset($_GET['supprimer']))
                delDepartement($db, $_GET['supprimer']);
            break;
        case 'dominantes':
            $active = array('', '', '', 'active','');
            if (isset($_GET['supprimer']))
                delDominante($db, $_GET['supprimer']);
            break;
        case 'reinit':
            $active = array('', '', '', '','active');
            if (isset($_GET['supprimer']))
                delAll($db,$_GET['supprimer']);
            break;
        default:
            $active = array('', '', '', '','');
            break;
    }
}
nav("Tableau de bord");
headerBig("Tableau de bord");
if(isset($_GET['modif'])){
    if (isset($_GET['menu'])) {
        echo"
        <section class='mt-3'>
        <div class='container'>
            <div class='row'>";
        switch ($_GET['menu']) {
            case 'personnes':
                $personne = getPersonne($db,$_GET['modif']);
                echo"
                <div class='col-2'></div>
                    <div class='col-8'>
                        <form action='tableau_de_bord.php?menu=personnes' method='post'>
                            <!-- Fonction input -->
                            <div class='form-outline mb-4'>
                                <select class='select' name='fonction'>
                                    <option value='etudiant'>etudiant</option>
                                    <option value='enseignant'>enseignant</option>
                                </select>
                            <label class='form-label'>Fonction</label>
                            </div>
                            </div>
                            <!-- Submit button -->
                            <button type='submit' class='btn btn-primary btn-block mb-4' name='modifPersonne' value='".$_GET['modif']."' >Modifier</button>
                        </form>
                    </div>
                <div class='col-2'></div>";
                break;
        }
    }
}else{
    echo "
    <section class='mt-3'>
        <h2 class='text-center'>Données de la base de données</h2>
        <div class='container'>
            <div class='row'>
                <div class='col-md-4 col-lg-2 col-sm-4' id='divAdmin'>
                    <nav class='nav nav-pills nav-fill flex-column flex-sm-column mb-3'>
                        <a class='nav-link " . $active[0] . "' id='personnes' href='tableau_de_bord.php?menu=personnes'>Personnes</a>
                        <a class='nav-link " . $active[1] . "' id='sujets' href='tableau_de_bord.php?menu=sujets'>Sujets</a>
                        <a class='nav-link " . $active[2] . "' id='departements' href='tableau_de_bord.php?menu=departements'>Departements</a>
                        <a class='nav-link " . $active[3] . "' id='dominantes' href='tableau_de_bord.php?menu=dominantes'>Dominantes</a>
                        <a class='nav-link " . $active[4] . "' id='reinit' href='tableau_de_bord.php?menu=reinit'>Réinitialisation</a>
                    </nav>
                </div>
                <div class='col-md-8 col-lg-10 col-sm-8'>
                    <div class='table-responsive'>
                        <table class='table table-striped table-bordered table-hover'>";
    if (isset($_GET['menu'])) {
        switch ($_GET['menu']) {
            case 'personnes':
                echo "
                        <caption>Liste des personnes</caption>
                        <thead>
                            <tr>
                                <th class='sortable-numeric' scope='col'>Nombres lignes</th>
                                <th scope='col'>Id personne</th>
                                <th scope='col'>Nom</th>
                                <th scope='col'>Prénom</th>
                                <th scope='col'>Date de naissance</th>
                                <th scope='col'>Identifiant</th>
                                <th scope='col'>Mot de passe</th>
                                <th scope='col'>Fonction</th>
                                <th scope='col' colspan='2'>Actions</th>
                            </tr>
                        </thead>
                    <tbody>";
                $personnes = getAllPersonnes($db);
                $cpt = 1;
                foreach ($personnes as $personne) {
                    echo "
                        <tr>
                            <th scope='row'>$cpt</th>
                            <td>" . $personne['id_personne'] . "</td>
                            <td>" . $personne['nom'] . "</td>
                            <td>" . $personne['prenom'] . "</td>
                            <td>" . $personne['date_naissance'] . "</td>
                            <td>" . $personne['identifiant'] . "</td>";
                            echo "<td>" . substr( $personne['mot_de_passe'], 0, 20 ) . "...</td>";
                            echo"<td>" . $personne['fonction'] . "</td>
                            <td class='width-1vw'>";
                if($personne['fonction'] == 'etudiant'){
                    echo"<img class='img-fluid click' onclick= 'modifierPersonne(" . $personne['id_personne'] . ")' src='/grp_9_5/dist/assets/img/tableau_de_bord/edit.png' title='modif' alt='modifier'>";
                    echo"
                    <noscript>
                        <a href='tableau_de_bord.php?menu=personnes&modif=".$personne['id_personne']."'>Modifier sans JS</a>
                    </noscript>";
                }
            echo"
                            </td>
                            <td class='width-1vw'>
                                <img class='img-fluid click' onclick='supprimerPersonne(" . $personne['id_personne'] . ")' src='/grp_9_5/dist/assets/img/tableau_de_bord/supprimer.png' title='suppr' alt='supprimer'>";
                                echo"
                                <noscript>
                                    <a href='tableau_de_bord.php?menu=personnes&supprimer=".$personne['id_personne']."'>Supprimer sans JS</a>
                                </noscript>";
                            echo"
                            </td>
                        </tr>";
                    $cpt++;
                }
                echo"
                <tr>
                    <td colspan='10'><a href='inscription_enseignant.php'>Ajouter un enseignant</a></td>
                </tr>";
                break;
            case 'sujets':
                echo "<caption>Liste des sujets</caption>
                        <thead>
                            <tr>
                                <th scope='col'>Nombres lignes</th>
                                <th scope='col'>Id sujet</th>
                                <th scope='col'>Département rattaché</th>
                                <th scope='col'>Dominante</th>
                                <th scope='col'>Nom sujet</th>
                                <th scope='col'>Résumé</th>
                                <th scope='col'>Nombres de places restantes</th>
                                <th scope='col'>Pièce jointe</th>
                                <th scope='col'>Actions</th>
                            </tr>
                        </thead>
                    <tbody>";
                $sujets = getAllSujets($db);
                $cpt = 1;
                foreach ($sujets as $sujet) {
                    echo "
                    <tr>
                        <th scope='row'>$cpt</th>
                        <td>" . $sujet['id_sujet'] . "</td>
                        <td>" . getDepartement($db,$sujet['departement'])['nom_departement'] . "</td>
                        <td>" . getDominante($db,$sujet['dominante'])['nom_dominante'] . "</td>
                        <td>" . $sujet['nom_sujet'] . "</td>
                        <td>" . $sujet['resume_sujet'] . "</td>
                        <td>" . $sujet['nb_places_rest'] . "</td>
                        <td>" . $sujet['chemin_piece_jointe'] . "</td>
                        <td class='width-1vw'>
                            <img class='img-fluid click' onclick='supprimerSujet(" . $sujet['id_sujet'] . ")' src='/grp_9_5/dist/assets/img/tableau_de_bord/supprimer.png' title='suppr' alt='supprimer'>
                        </td>
                    </tr>";
                    $cpt++;
                }
                break;
            case 'departements':
                echo "<caption>Liste des départements</caption>
                        <thead>
                            <tr>
                                <th scope='col'>Nombres lignes</th>
                                <th scope='col'>id departement</th>
                                <th scope='col'>nom departement</th>
                                <th scope='col'>Actions</th>
                            </tr>
                        </thead>
                    <tbody>";
                $departements = getAllDepartements($db);
                $cpt = 1;
                foreach ($departements as $departement) {
                    echo "<tr>
                            <th scope='row'>$cpt</th>
                            <td>" . $departement['id_departement'] . "</td>
                            <td>" . $departement['nom_departement'] . "</td>
                            <td class='width-1vw'>
                                <img class='img-fluid click' onclick='supprimerDepartement(" . $departement['id_departement'] . ")' src='/grp_9_5/dist/assets/img/tableau_de_bord/supprimer.png' title='suppr' alt='supprimer'>
                            </td>
                        </tr>";
                    $cpt++;
                }
                break;
                case 'dominantes':
                    echo "<caption>Liste des dominantes</caption>
                            <thead>
                                <tr>
                                    <th scope='col'>Nombres lignes</th>
                                    <th scope='col'>id dominante</th>
                                    <th scope='col'>nom dominante</th>
                                    <th scope='col'>Departement rattaché</th>
                                    <th scope='col'>Actions</th>
                                </tr>
                            </thead>
                        <tbody>";
                    $dominantes = getAllDominantes($db);
                    $cpt = 1;
                    foreach ($dominantes as $dominante) {
                        echo "<tr>
                        <th scope='row'>$cpt</th>
                        <td>" . $dominante['id_dominante'] . "</td>
                        <td>" . $dominante['nom_dominante'] . "</td>
                        <td>" . getDepartement($db,$dominante['departement'])['nom_departement'] . "</td>
                        <td class='width-1vw'>
                            <img class='img-fluid click' onclick='supprimerDominante(" . $dominante['id_dominante'] . ")' src='/grp_9_5/dist/assets/img/tableau_de_bord/supprimer.png' title='suppr' alt='supprimer'>
                        </td>
                        </tr>";
                        $cpt++;
                    }
                    break;
                case 'reinit':
                    // Parametre remplacer par des numéros au lieu du nom de la table (erreur js)
                    echo"
                        <tbody>
                            <tr>
                                <td>
                                    <button class='btn btn-warning click' onclick='reinitialiserTable(0)'> Réinitialiser tous les sujets</button>
                                </td>
                                <td>
                                    <button class='btn btn-warning click' onclick='reinitialiserTable(1)'> Réinitialiser toutes les personnes</button>
                                </td>
                                <td>
                                    <button class='btn btn-warning click' onclick='reinitialiserTable(2)'> Réinitialiser tous les départements</button>
                                </td>
                                <td>
                                    <button class='btn btn-warning click' onclick='reinitialiserTable(3)'> Réinitialiser toutes les dominantes</button>
                                <td>
                            </tr>";
                break;
            default:
                echo "<div>Veuillez choisir les données à afficher</div>";
                break;
        }
    }else{
        echo "<div>Veuillez choisir les données à afficher</div>";
    }
    echo "
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>";
}
echo"
</section>";
pied();
?>