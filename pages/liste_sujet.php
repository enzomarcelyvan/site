<?php
include('../scripts/function.php');
include('../scripts/db.php');
enTete("Liste sujets");
echo "
  <body>";
nav("Liste sujets");
headerBig("Liste sujets");
if(!isset($_SESSION['identifiant'])){
    header('Location: /grp_9_5/index.php');
    exit;
}

$db = initDb();
echo"
    <!-- listeSujets Section-->
    <section class='page-section listeSujets' id='listeSujets'>
        <h2 class='text-center'>Sujets regroupé par dominantes</h2>
        <div class='container'>
            <!-- listeSujets Grid Items-->
            <div class='row justify-content-center'>";
$personne = getPersonneByIdentifiant($db,$_SESSION['identifiant']);
if($personne['fonction']=='etudiant'){
    $departement = getDepartement($db,getDepartementEtudiant($db,$personne['id_personne'])[0]);
    $cpt = 1;
    $dominantes = getAllDominantesByDepartement($db,$departement['id_departement']);
    foreach($dominantes as $dominante){
        echo"
                <!-- listeSujets Item $cpt-->
                <div class='col-md-6 col-lg-4 mb-5 mt-5'>
                    <div class='listeSujets-item mx-auto' data-bs-toggle='modal' data-bs-target='#listeSujetsModal$cpt'>
                        <div class='listeSujets-item-caption d-flex align-items-center justify-content-center h-100 w-100'>
                            <div class='listeSujets-item-caption-content text-center text-white'><i class='fas fa-plus fa-3x'></i></div>
                        </div>
                        <img class='img-fluid' src='../dist/assets/img/listeSujets/".$dominante['nom_dominante'].".jpg' alt='dominante ".$dominante['nom_dominante']."' />
                        <h2>Dominante ".$dominante['nom_dominante']."</h2>
                    </div>
                </div>";
        $cpt++;
    }

    echo"
            </div>
        </div>
        <!-- listeSujets Modals-->";
    $cpt = 1;
    foreach($dominantes as $dominante){
    echo"
        <!-- listeSujets Modal $cpt-->
        <div class='listeSujets-modal modal fade' id='listeSujetsModal$cpt' tabindex='-1' aria-labelledby='listeSujetsModal$cpt' aria-hidden='true'>
            <div class='modal-dialog modal-xl'>
                <div class='modal-content'>
                    <div class='modal-header border-0'><button class='btn-close' type='button' data-bs-dismiss='modal' aria-label='Close'></button></div>
                        <div class='modal-body text-center pb-5'>
                            <div class='container'>
                                <div class='row justify-content-center'>
                                    <div class='col-lg-8'>
                                        <!-- listeSujets Modal - Title-->
                                        <h2 class='listeSujets-modal-title text-secondary text-uppercase mb-0'>Sujets - Dominante ".$dominante['nom_dominante']."</h2>
                                        <!-- Icon Divider-->
                                        <div class='divider-custom'>
                                            <div class='divider-custom-line'></div>
                                            <div class='divider-custom-icon'><i class='fas fa-star'></i></div>
                                            <div class='divider-custom-line'></div>
                                        </div>
                                        <!-- listeSujets Modal - Image-->
                                        <img class='img-fluid rounded mb-5' src='../dist/assets/img/listeSujets/".$dominante['nom_dominante'].".jpg' alt='dominante ".$dominante['nom_dominante']."' />
                                        <!-- listeSujets Modal - Liste-->
                                        <div class='table-responsive'>
                                            <table class='table table-striped table-bordered table-hover'>
                                                <caption>Liste des sujets</caption>
                                                <thead>
                                                <tr>
                                                    <th scope='col'>Nombres lignes</th>
                                                    <th scope='col'>Département rattaché</th>
                                                    <th scope='col'>Dominante</th>
                                                    <th scope='col'>Nom sujet</th>
                                                    <th scope='col'>Résumé</th>
                                                    <th scope='col'>Places restantes</th>
                                                    <th scope='col'>Inscription</th>
                                                </tr>
                                                </thead>
                                                <tbody>";
                                                $sujets = getAllSujetsByDominante($db,$dominante['id_dominante']);
                                                $nbLignes = 1;
                                                foreach($sujets as $sujet){
                                                    echo"<tr>
                                                        <th scope='row'>$nbLignes</th>
                                                        <td>" . getDepartement($db,$sujet['departement'])['nom_departement'] . "</td>
                                                        <td>" . getDominante($db,$sujet['dominante'])['nom_dominante'] . "</td>
                                                        <td>" . $sujet['nom_sujet'] . "</td>
                                                        <td>" . $sujet['resume_sujet'] . "</td>
                                                        <td>" . $sujet['nb_places_rest'] . "</td>
                                                        <td><a href='sujet_detaille.php?sujet=".$sujet['id_sujet']."' class='link-primary'>Page détaillé</a></td>
                                                    </tr>";
                                                    $nbLignes++;
                                                }
                                                echo"</tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>";
    $cpt++;
    }
}else{
    $dominantes = getAllDominantes($db);
    $cpt=1;
    foreach($dominantes as $dominante){
        echo"
                    <!-- listeSujets Item $cpt-->
                    <div class='col-md-6 col-lg-4 mb-5 mt-5'>
                        <div class='listeSujets-item mx-auto' data-bs-toggle='modal' data-bs-target='#listeSujetsModal$cpt'>
                            <div class='listeSujets-item-caption d-flex align-items-center justify-content-center h-100 w-100'>
                                <div class='listeSujets-item-caption-content text-center text-white'><i class='fas fa-plus fa-3x'></i></div>
                            </div>
                            <img class='img-fluid' src='../dist/assets/img/listeSujets/".$dominante['nom_dominante'].".jpg' alt='dominante ".$dominante['nom_dominante']."' />
                            <h2>Dominante ".$dominante['nom_dominante']."</h2>
                        </div>
                    </div>";
        $cpt++;
    }

    echo"
                    <!-- listeSujets Modals-->";
    $cpt = 1;
    foreach($dominantes as $dominante){
    echo"
                <!-- listeSujets Modal $cpt-->
                <div class='listeSujets-modal modal fade' id='listeSujetsModal$cpt' tabindex='-1' aria-labelledby='listeSujetsModal$cpt' aria-hidden='true'>
                    <div class='modal-dialog modal-xl'>
                        <div class='modal-content'>
                            <div class='modal-header border-0'><button class='btn-close' type='button' data-bs-dismiss='modal' aria-label='Close'></button></div>
                            <div class='modal-body text-center pb-5'>
                                <div class='container'>
                                    <div class='row justify-content-center'>
                                        <div class='col-lg-8'>
                                            <!-- listeSujets Modal - Title-->
                                            <h2 class='listeSujets-modal-title text-secondary text-uppercase mb-0'>Sujets - Dominante ".$dominante['nom_dominante']."</h2>
                                            <!-- Icon Divider-->
                                            <div class='divider-custom'>
                                                <div class='divider-custom-line'></div>
                                                <div class='divider-custom-icon'><i class='fas fa-star'></i></div>
                                                <div class='divider-custom-line'></div>
                                            </div>
                                            <!-- listeSujets Modal - Image-->
                                            <img class='img-fluid rounded mb-5' src='../dist/assets/img/listeSujets/".$dominante['nom_dominante'].".jpg' alt='dominante ".$dominante['nom_dominante']."' />
                                            <!-- listeSujets Modal - Liste-->
                                            <div class='table-responsive'>
                                                <table class='table table-striped table-bordered table-hover'>
                                                    <caption>Liste des sujets</caption>
                                                    <thead>
                                                    <tr>
                                                        <th scope='col'>Nombres lignes</th>
                                                        <th scope='col'>Département rattaché</th>
                                                        <th scope='col'>Dominante</th>
                                                        <th scope='col'>Nom sujet</th>
                                                        <th scope='col'>Résumé</th>
                                                        <th scope='col'>Places restantes</th>
                                                        <th scope='col'>Inscription</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>";
                                                    $sujets = getAllSujetsByDominante($db,$dominante['id_dominante']);
                                                    $nbLignes = 1;
                                                    foreach($sujets as $sujet){
                                                        echo"<tr>
                                                            <th scope='row'>$nbLignes</th>
                                                            <td>" . getDepartement($db,$sujet['departement'])['nom_departement'] . "</td>
                                                            <td>" . getDominante($db,$sujet['dominante'])['nom_dominante'] . "</td>
                                                            <td>" . $sujet['nom_sujet'] . "</td>
                                                            <td>" . $sujet['resume_sujet'] . "</td>
                                                            <td>" . $sujet['nb_places_rest'] . "</td>
                                                            <td><a href='sujet_detaille.php?sujet=".$sujet['id_sujet']."' class='link-primary'>Page détaillé</a></td>
                                                        </tr>";
                                                        $nbLignes++;
                                                    }
                                                    echo"</tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>";
    $cpt++;
    }
}
echo"   
    </section>";

//S'il n'y a pas de javascript
echo "
<noscript>
    <section class='page-section m-2'>
        <h2> Veuillez autorisez javascript pour un meilleur rendu </h2>";
$personne = getPersonneByIdentifiant($db,$_SESSION['identifiant']);
if($personne['fonction']=='etudiant'){
    $departement = getDepartement($db,getDepartementEtudiant($db,$personne['id_personne'])[0]);
    $cpt = 1;
    $dominantes = getAllDominantesByDepartement($db,$departement['id_departement']);
    echo"
        <!-- listeSujets Modals-->";
    $cpt = 1;
    foreach($dominantes as $dominante){
    echo"
        <div class='container'>
            <div class='row justify-content-center'>
                <div class='col-lg-8'>
                    <!-- listeSujets Modal - Title-->
                    <h2 class='listeSujets-modal-title text-secondary text-uppercase mb-0'>Sujets - Dominante ".$dominante['nom_dominante']."</h2>
                    <!-- Icon Divider-->
                    <div class='divider-custom'>
                        <div class='divider-custom-line'></div>
                        <div class='divider-custom-icon'><i class='fas fa-star'></i></div>
                        <div class='divider-custom-line'></div>
                    </div>
                    <!-- listeSujets Modal - Image-->
                    <img class='img-fluid rounded mb-5' src='../dist/assets/img/listeSujets/".$dominante['nom_dominante'].".jpg' alt='dominante ".$dominante['nom_dominante']."' />
                    <!-- listeSujets Modal - Liste-->
                    <div class='table-responsive'>
                        <table class='table table-striped table-bordered table-hover'>
                            <caption>Liste des sujets</caption>
                            <thead>
                            <tr>
                                <th scope='col'>Nombres lignes</th>
                                <th scope='col'>Département rattaché</th>
                                <th scope='col'>Dominante</th>
                                <th scope='col'>Nom sujet</th>
                                <th scope='col'>Résumé</th>
                                <th scope='col'>Places restantes</th>
                                <th scope='col'>Inscription</th>
                            </tr>
                            </thead>
                            <tbody>";
                            $sujets = getAllSujetsByDominante($db,$dominante['id_dominante']);
                            $nbLignes = 1;
                            foreach($sujets as $sujet){
                                echo"<tr>
                                    <th scope='row'>$nbLignes</th>
                                    <td>" . getDepartement($db,$sujet['departement'])['nom_departement'] . "</td>
                                    <td>" . getDominante($db,$sujet['dominante'])['nom_dominante'] . "</td>
                                    <td>" . $sujet['nom_sujet'] . "</td>
                                    <td>" . $sujet['resume_sujet'] . "</td>
                                    <td>" . $sujet['nb_places_rest'] . "</td>
                                    <td><a href='sujet_detaille.php?sujet=".$sujet['id_sujet']."' class='link-primary'>Page détaillé</a></td>
                                </tr>";
                                $nbLignes++;
                            }
                            echo"</tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>";
    $cpt++;
    }
}else{
    echo"
        <!-- listeSujets Modals-->
        <div class='container'>";
    $cpt = 1;
    foreach($dominantes as $dominante){
    echo"                       
            <div class='row justify-content-center'>
                <div class='col-lg-8'>
                    <!-- listeSujets Modal - Title-->
                    <h2 class='listeSujets-modal-title text-secondary text-uppercase mb-0'>Sujets - Dominante ".$dominante['nom_dominante']."</h2>
                    <!-- Icon Divider-->
                    <div class='divider-custom'>
                        <div class='divider-custom-line'></div>
                        <div class='divider-custom-icon'><i class='fas fa-star'></i></div>
                        <div class='divider-custom-line'></div>
                    </div>
                    <!-- listeSujets Modal - Image-->
                    <img class='img-fluid rounded mb-5' src='../dist/assets/img/listeSujets/".$dominante['nom_dominante'].".jpg' alt='dominante ".$dominante['nom_dominante']."' />
                    <!-- listeSujets Modal - Liste-->
                    <div class='table-responsive'>
                        <table class='table table-striped table-bordered table-hover'>
                            <caption>Liste des sujets</caption>
                            <thead>
                            <tr>
                                <th scope='col'>Nombres lignes</th>
                                <th scope='col'>Département rattaché</th>
                                <th scope='col'>Dominante</th>
                                <th scope='col'>Nom sujet</th>
                                <th scope='col'>Résumé</th>
                                <th scope='col'>Places restantes</th>
                                <th scope='col'>Inscription</th>
                            </tr>
                            </thead>
                            <tbody>";
                            $sujets = getAllSujetsByDominante($db,$dominante['id_dominante']);
                            $nbLignes = 1;
                            foreach($sujets as $sujet){
                                echo"<tr>
                                    <th scope='row'>$nbLignes</th>
                                    <td>" . getDepartement($db,$sujet['departement'])['nom_departement'] . "</td>
                                    <td>" . getDominante($db,$sujet['dominante'])['nom_dominante'] . "</td>
                                    <td>" . $sujet['nom_sujet'] . "</td>
                                    <td>" . $sujet['resume_sujet'] . "</td>
                                    <td>" . $sujet['nb_places_rest'] . "</td>
                                    <td><a href='sujet_detaille.php?sujet=".$sujet['id_sujet']."' class='link-primary'>Page détaillé</a></td>
                                </tr>";
                                $nbLignes++;
                            }
                            echo"</tbody>
                            </table>
                        </div>
                    </div>
                </div>";
    $cpt++;
    }
}
echo"
    </section>
</noscript>";
pied();
?>