<?php
  function initDb(){
    try {
      $db = new PDO('mysql:host=localhost;dbname=bdd_9_5;charset=utf8', 'grp_9_5', 'Reigoh4Iet',[PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
      //$db = new PDO('mysql:host=localhost;dbname=bdd_9_5;charset=utf8', 'root', '',[PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
    } catch (Exception $e) {
      die('Erreur : ' . $e->getMessage());
    }
    return $db;
  }

  /**********************************/
  /* Fonctions pour les personnes  */
  /********************************/ 
  function addPersonne($db,$arrayPersonne){
    $sqlQuery = 'INSERT INTO Personne VALUES (NULL, ?, ?, ?, ?, ?, ?)';
    $personnesStatement = $db->prepare($sqlQuery);
    $personnesStatement->execute($arrayPersonne);
  }
  function updatePersonne($db,$fonction,$id_personne){
    if(getEtudiant($db,$id_personne)['sujet']!=null){
      updatePlacesSujet($db,getEtudiant($db,$id_personne)['sujet'], '+');
    }
    delEtudiant($db,$id_personne);
    addEnseignant($db,$id_personne);
    $sqlQuery = "UPDATE Personne SET fonction =  ? WHERE id_personne = ?";
    $dominanteStatement = $db->prepare($sqlQuery);
    $dominanteStatement->execute(array($fonction,$id_personne));
  }
  function getAllPersonnes($db){
    $sqlQuery = 'SELECT * FROM Personne';
    $personnesStatement = $db->prepare($sqlQuery);
    $personnesStatement->execute();
    $personnes = $personnesStatement->fetchAll();

    return $personnes;
  }
  function getPersonne($db,$identifiant){
    $sqlQuery = "SELECT * FROM Personne WHERE id_personne = ?";
    $personneStatement = $db->prepare($sqlQuery);
    $personneStatement->execute(array($identifiant));
    $personne = $personneStatement->fetchAll();
    return $personne[0];
  }
  function getPersonneByIdentifiant($db,$identifiant){
    $sqlQuery = "SELECT * FROM Personne WHERE identifiant = ?";
    $personneStatement = $db->prepare($sqlQuery);
    $personneStatement->execute(array($identifiant));
    $personne = $personneStatement->fetchAll();
    return $personne[0];
  }

  function delPersonne($db,$identifiant){
    if(getPersonne($db,$identifiant)['fonction'] == 'enseignant'){
      $sqlQuery = "DELETE FROM Enseignant WHERE id_enseignant = ?";
      $personneStatement = $db->prepare($sqlQuery);
      $personneStatement->execute(array($identifiant));
    }else if(getPersonne($db,$identifiant)['fonction'] == 'etudiant'){
      $sqlQuery = "DELETE FROM Etudiant WHERE id_etudiant = ?";
      $personneStatement = $db->prepare($sqlQuery);
      $personneStatement->execute(array($identifiant));
    }
    $sqlQuery = "DELETE FROM Personne WHERE id_personne = ?";
    $personneStatement = $db->prepare($sqlQuery);
    $personneStatement->execute(array($identifiant));
  }

  function getAllIdentifiants($db){
    $sqlQuery = 'SELECT identifiant FROM Personne';
    $personnesStatement = $db->prepare($sqlQuery);
    $personnesStatement->execute();
    $identifiants = $personnesStatement->fetchAll();

    return $identifiants;
  }
  /**********************************/
  /* Fonctions pour les sujets     */
  /********************************/ 
  function addSujet($db, $arraySujet){
    $sqlQuery='INSERT INTO Sujet Values (null,?,?,?,?,?,?)';
    $personneStatement = $db->prepare($sqlQuery);
    $personneStatement->execute($arraySujet);
  }

  function getAllSujets($db){
    $sqlQuery = 'SELECT * FROM Sujet';
    $personnesStatement = $db->prepare($sqlQuery);
    $personnesStatement->execute();
    $sujets = $personnesStatement->fetchAll();

    return $sujets;
  }
  function getSujet($db,$identifiant){
    $sqlQuery = "SELECT * FROM Sujet WHERE id_sujet = ?";
    $sujetStatement = $db->prepare($sqlQuery);
    $sujetStatement->execute(array($identifiant));
    $sujet = $sujetStatement->fetchAll();
    return $sujet[0];
  }
  function getDominanteSujet($db,$dominante){
    $sqlQuery = "SELECT dominante FROM Sujet WHERE dominante = ?";
    $sujetStatement = $db->prepare($sqlQuery);
    $sujetStatement->execute(array($dominante));
    $sujet = $sujetStatement->fetchAll();
    return $sujet[0];
  }
  function getDepartementSujet($db,$departement){
    $sqlQuery = "SELECT departement FROM Sujet WHERE departement = ?";
    $sujetStatement = $db->prepare($sqlQuery);
    $sujetStatement->execute(array($departement));
    $sujet = $sujetStatement->fetchAll();
    return $sujet[0];
  }
  function delSujet($db,$identifiant){
    $sqlQuery = "DELETE FROM Sujet WHERE id_sujet = ?";
    $personneStatement = $db->prepare($sqlQuery);
    $personneStatement->execute(array($identifiant));
  }
  function updateSujet($db,$departement,$dominante,$nomSujet,$resumeSujet,$nbPlacesRestantes,$cheminPieceJointe,$idSujet){
    $sqlQuery = "UPDATE Sujet SET departement=?, dominante=?, nom_sujet= ?, resume_sujet=?, nb_places_rest=?, chemin_piece_jointe=? WHERE id_sujet=?";
    $sujetStatement = $db->prepare($sqlQuery);
    $sujetStatement->execute(array($departement,$dominante,$nomSujet,$resumeSujet,$nbPlacesRestantes,$cheminPieceJointe,$idSujet));
  }
  /************************************/
  /* Fonction pour les départements  */
  /**********************************/ 
  function getAllDepartements($db){
    $sqlQuery = 'SELECT * FROM Departement';
    $departementsStatement = $db->prepare($sqlQuery);
    $departementsStatement->execute();
    $departements = $departementsStatement->fetchAll();
    return $departements;
  }
  function getDepartement($db,$identifiant){
    $sqlQuery = "SELECT * FROM Departement WHERE id_departement = ?";
    $departementStatement = $db->prepare($sqlQuery);
    $departementStatement->execute(array($identifiant));
    $departement = $departementStatement->fetchAll();
    return $departement[0];
  }
  function getDepartementByName($db, $nom){
    $sqlQuery = "SELECT * FROM Departement WHERE nom_departement = ?";
    $departementStatement = $db->prepare($sqlQuery);
    $departementStatement->execute(array($nom));
    $departement = $departementStatement->fetchAll();
    return $departement[0];
  }
  function delDepartement($db,$identifiant){
    $sqlQuery = "DELETE FROM Departement WHERE id_departement = ?";
    $departementStatement = $db->prepare($sqlQuery);
    $departementStatement->execute(array($identifiant));
  }
  /************************************/
  /* Fonction pour les étudiants     */
  /**********************************/ 
  function getEtudiant($db,$identifiant){
    $sqlQuery = "SELECT * FROM Etudiant WHERE id_etudiant = ?";
    $etudiantStatement = $db->prepare($sqlQuery);
    $etudiantStatement->execute(array($identifiant));
    $etudiant = $etudiantStatement->fetchAll();
    return $etudiant[0];
  }
  function getDepartementEtudiant($db,$identifiant){
    $sqlQuery = "SELECT departement FROM Etudiant WHERE id_etudiant = ?";
    $departementStatement = $db->prepare($sqlQuery);
    $departementStatement->execute(array($identifiant));
    $departement = $departementStatement->fetchAll();
    return $departement[0];
  }
  function getSujetEtudiant($db,$identifiant){
    $sqlQuery = "SELECT sujet FROM Etudiant WHERE id_etudiant = ?";
    $sujetStatement = $db->prepare($sqlQuery);
    $sujetStatement->execute(array($identifiant));
    $sujet = $sujetStatement->fetchAll();
    return $sujet[0];
  }
  function addEtudiant($db,$arrayEtud){
    $sqlQuery = 'INSERT INTO Etudiant VALUES (?,?,?)';
    $personnesStatement = $db->prepare($sqlQuery);
    $personnesStatement->execute($arrayEtud);
  }
  function delEtudiant($db,$identifiant){
    $sqlQuery = "DELETE FROM Etudiant WHERE id_etudiant = ?";
    $etudiantStatement = $db->prepare($sqlQuery);
    $etudiantStatement->execute(array($identifiant));
  }
  /************************************/
  /* Fonction pour les enseignants   */
  /**********************************/
  function getEnseignant($db,$identifiant){
    $sqlQuery = "SELECT * FROM Enseignant WHERE id_enseignant = ?";
    $enseignantStatement = $db->prepare($sqlQuery);
    $enseignantStatement->execute(array($identifiant));
    $enseignant = $enseignantStatement->fetchAll();
    return $enseignant[0];
  }
  function getEnseignantFromId($db,$identifiant){
    $sqlQuery = "SELECT id_personne FROM Personne WHERE identifiant = ?";
    $personneStatement = $db->prepare($sqlQuery);
    $personneStatement->execute(array($identifiant));
    $enseignantId=$personneStatement->fetchAll();

    $sqlQuery = "SELECT * FROM Enseignant WHERE id_enseignant = ?";
    $enseignantStatement = $db->prepare($sqlQuery);
    $enseignantStatement->execute(array($enseignantId[0]['id_personne']));
    $enseignant = $enseignantStatement->fetchAll();
    if($enseignant!=null){
      return $enseignant[0];
    }
    return null;
  }
  function getEnseignantBySujet($db,$identifiant){
    $sqlQuery = "SELECT * FROM Enseignant WHERE mon_sujet = ?";
    $enseignantStatement = $db->prepare($sqlQuery);
    $enseignantStatement->execute(array($identifiant));
    $enseignant = $enseignantStatement->fetchAll();
    return $enseignant[0];
  }
  function addEnseignant($db,$identifiant){
    $sqlQuery = 'INSERT INTO Enseignant VALUES (?,?)';
    $personnesStatement = $db->prepare($sqlQuery);
    $personnesStatement->execute(array($identifiant,null));
  }
  function addSujetEnseignant($db,$identifiant,$idSujet){
    $sqlQuery = "INSERT INTO Enseignant VALUES (?,?)";
    $personneStatement =$db->prepare($sqlQuery);
    $personneStatement->execute(array($identifiant,$idSujet));
  }
  function isEnseignantHasSujet($db, $identifiant){
    $sqlQuery = 'SELECT id_enseignant FROM Enseignant';
    $enseignantStatement = $db->prepare($sqlQuery);
    $enseignantStatement->execute();
    $enseignant = $enseignantStatement->fetchAll();
    foreach($enseignant as $id){
      if($id['id_enseignant']==$identifiant)
        return true;
    }
    return false;
  }
  /************************************/
  /* Fonction pour les dominantes    */
  /**********************************/ 
  function getAllDominantes($db){
    $sqlQuery = 'SELECT * FROM Dominante';
    $dominanteStatement = $db->prepare($sqlQuery);
    $dominanteStatement->execute();
    $dominantes = $dominanteStatement->fetchAll();

    return $dominantes;
  }
  function getAllDominantesByDepartement($db,$identifiant){
    $sqlQuery = 'SELECT * FROM Dominante WHERE departement = ?';
    $dominanteStatement = $db->prepare($sqlQuery);
    $dominanteStatement->execute(array($identifiant));
    $dominantes = $dominanteStatement->fetchAll();
    return $dominantes;
  }
  function getDominante($db,$identifiant){
    $sqlQuery = "SELECT * FROM Dominante WHERE id_dominante = ?";
    $dominanteStatement = $db->prepare($sqlQuery);
    $dominanteStatement->execute(array($identifiant));
    $dominante = $dominanteStatement->fetchAll();
    return $dominante[0];
  }
  function getDominanteByName($db, $nom){
    $sqlQuery = "SELECT * FROM Dominante WHERE nom_dominante = ?";
    $dominanteStatement = $db->prepare($sqlQuery);
    $dominanteStatement->execute(array($nom));
    $dominante = $dominanteStatement->fetchAll();
    return $dominante[0];
  }
  function getAllSujetsByDominante($db,$identifiant){
    $sqlQuery = 'SELECT * FROM Sujet WHERE dominante = ?';
    $sujetStatement = $db->prepare($sqlQuery);
    $sujetStatement->execute(array($identifiant));
    $sujets = $sujetStatement->fetchAll();

    return $sujets;
  }
  function delDominante($db,$identifiant){
    $sqlQuery = "DELETE FROM Dominante WHERE id_dominante = ?";
    $dominanteStatement = $db->prepare($sqlQuery);
    $dominanteStatement->execute(array($identifiant));
  }
  /************************************/
  /* Autres Fonctions                */
  /**********************************/ 
  function delAll($db,$table){
    $sqlQuery = "DELETE FROM ".$table;
    $delStatement = $db->prepare($sqlQuery);
    $delStatement->execute();
  }
  function inscrireEtudiant($db,$id_sujet,$id_etudiant){
    $sqlQuery = "UPDATE Etudiant SET sujet =  ? WHERE id_etudiant = ?";
    $dominanteStatement = $db->prepare($sqlQuery);
    $dominanteStatement->execute(array($id_sujet,$id_etudiant));
    updatePlacesSujet($db,$id_sujet,'-');
  }
  function delSujetFromEtudiant($db,$identifiant,$id_sujet){
    $sqlQuery = "UPDATE Etudiant SET sujet =  null WHERE id_etudiant = ?;";
    $dominanteStatement = $db->prepare($sqlQuery);
    $dominanteStatement->execute(array($identifiant));
    updatePlacesSujet($db,$id_sujet,'+');
  }
  function updatePlacesSujet($db,$id_sujet,$operation){
    $sqlQuery ="UPDATE Sujet SET nb_places_rest =  nb_places_rest".$operation."1 WHERE id_sujet = ?";
    $dominanteStatement = $db->prepare($sqlQuery);
    $dominanteStatement->execute(array($id_sujet));
  }
  function getAllEtudiantInscrit($db,$identifiant){
    $sqlQuery = "SELECT * FROM Etudiant WHERE sujet = ?";
    $etudiantStatement = $db->prepare($sqlQuery);
    $etudiantStatement->execute(array($identifiant));
    $etudiant = $etudiantStatement->fetchAll();
    return $etudiant;
  }
  function getMaxSujetId($listeSujets){
    $maxId=0;
    foreach($listeSujets as $i){
      if($i['id_sujet']>=$maxId){
        $maxId=$i['id_sujet'];
      }
    }
    return $maxId;
  }
?>
