<?php 
    if($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST['deconnexion'])){
        deconnexion();
    }

    function enTete($nom_page){
      echo "<!DOCTYPE html>
<html lang='fr'>
  <head>
      <meta charset='utf-8' />
      <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no' />
      <meta name='description' content='site sujet ingénieurs' />
      <meta name='author' content='enzomarcelyvan' />
      <title>$nom_page</title>
      <!-- Font Awesome icons (free version)-->
      <script src='https://use.fontawesome.com/releases/v5.15.3/js/all.js' crossorigin='anonymous'></script>
      <!-- Google fonts-->
      <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css' />
      <link href='https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic' rel='stylesheet' type='text/css' />
      <!-- Core theme CSS (includes Bootstrap)-->
      <link href='../dist/css/styles.css' rel='stylesheet' />
      <link rel='icon' href='/grp_9_5/dist/assets/favicon.ico' />
  </head>";
        session_start();
    }
    function pied($contact = false){
    if($contact){
        echo "
    <!-- Footer-->
    <footer class='footer text-center'>
        <div class='container'>
            <!-- Footer Location-->
            <h4 class='text-uppercase mb-4'>Adresse</h4>
            <p class='lead mb-0'>
                ESIGELEC - École d'ingénieurs généraliste
                <br />
                Technopôle du Madrillet, Av. Galilée, 76800 Saint-Étienne-du-Rouvray
            </p>
        </div>
    </footer>";}
    echo "
    <!-- Copyright Section-->
    <section>
        <div class='copyright py-4 text-center text-white'>
            <div class='container'><h6>Copyright &copy; Enzo Marcel Yvan 2021</h6></div>
        </div>
        <!-- Bootstrap core JS-->
        <script src='https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js'></script>
        <!-- Core theme JS-->
        <script src='/grp_9_5/dist/js/scripts.js'></script>
    </section>
  </body>
</html>";
    }

    function nav($page){
      if($page == 'Accueil'){
      echo "
    <nav class='navbar navbar-expand-lg bg-secondary text-uppercase fixed-top' id='mainNav'>
        <div class='container'>
            <a class='navbar-brand' href='#page-top'>Haut de page</a>
            <button class='navbar-toggler text-uppercase font-weight-bold bg-primary text-white rounded' type='button' data-bs-toggle='collapse' data-bs-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'>
                Menu
                <i class='fas fa-bars'></i>
            </button>
            <div class='collapse navbar-collapse' id='navbarResponsive'>
                <ul class='navbar-nav ms-auto'>
                    <li class='nav-item mx-0 mx-lg-1'>
                        <a class='nav-link py-3 px-0 px-lg-3 rounded' href='#listeSujets'>Les sujets</a>
                    </li>
                    <li class='nav-item mx-0 mx-lg-1'>
                        <a class='nav-link py-3 px-0 px-lg-3 rounded' href='#aPropos'>A propos</a>
                    </li>";
                    if(isset($_SESSION['fonction'])){
                        echo "<li class='nav-item mx-0 mx-lg-1'>
                            <a class='nav-link py-3 px-0 px-lg-3 rounded' href='./pages/home.php'>Mon compte</a>
                        </li>
                        <li>
                            <form action='index.php' method='post'>
                                <button class='navbar text-uppercase font-weight-bold bg-primary text-white rounded' type='submit' name='deconnexion' value='Deconnexion'>
                                    Déconnexion
                                </button>
                            </form>
                        </li>";
                    }else{
                        echo "
                        <li>
                            <a class='btn btn-primary m-2' href='./pages/connexion.php'>Connexion</a>
                        </li>
                        <li>
                            <a class='btn btn-primary m-2' href='./pages/inscription.php'>Inscription</a>
                        </li>";
                    }
                    echo "
                </ul>
            </div>
        </div>
    </nav>";
    // S'il n'y a pas javascript
    if(isset($_SESSION['fonction'])){
    echo "
    <noscript>
        <section class='page-section m-2'>
            <h2> Veuillez autorisez javascript pour un meilleur rendu </h2>
            <a class='nav-link py-3 px-0 px-lg-3 rounded' href='./pages/home.php'>Mon compte</a>
            <form action='home.php' method='post'>
                <button class='navbar text-uppercase font-weight-bold bg-primary text-white rounded' type='submit' name='deconnexion' value='Deconnexion'>
                    Déconnexion
                </button>
            </form>
        </section>
    </noscript>";
    }else{
    echo "
    <noscript>
        <section class='page-section m-2'>
            <h2> Veuillez autorisez javascript pour un meilleur rendu </h2>
            <a class='btn btn-primary m-2' href='./pages/connexion.php'>Connexion</a>
            <a class='btn btn-primary m-2' href='./pages/inscription.php'>Inscription</a>
        </section>
    </noscript>";
    }
      }else if ($page == 'Inscription' || $page == 'Connexion'){
        echo "
    <nav class='navbar navbar-expand-lg bg-secondary text-uppercase fixed-top' id='mainNav'>
        <div class='container'>
            <a class='navbar-brand' href='#page-top'>Haut de page</a>
            <button class='navbar-toggler text-uppercase font-weight-bold bg-primary text-white rounded' type='button' data-bs-toggle='collapse' data-bs-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'>
                Menu
                <i class='fas fa-bars'></i>
            </button>
            <div class='collapse navbar-collapse' id='navbarResponsive'>
                <ul class='navbar-nav ms-auto'>
                    <li class='nav-item mx-0 mx-lg-1'>
                        <a class='nav-link py-3 px-0 px-lg-3 rounded' href='https://moduleweb.esigelec.fr/grp_9_5/index.php'>Accueil</a>
                    </li>
                    <li>
                        <a class='btn btn-primary m-2' href='connexion.php'>Connexion</a>
                    </li>
                    <li>
                        <a class='btn btn-primary m-2' href='inscription.php'>Inscription</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>";
    echo "
    <noscript>
        <section class='page-section m-2'>
            <h2> Veuillez autorisez javascript pour un meilleur rendu </h2>
            <a class='nav-link py-3 px-0 px-lg-3 rounded' href='https://moduleweb.esigelec.fr/grp_9_5/index.php'>Accueil</a>
            <a class='btn btn-primary m-2' href='connexion.php'>Connexion</a>
            <a class='btn btn-primary m-2' href='inscription.php'>Inscription</a>
        </section>
    </noscript>";
      }
      else if($page == 'Home' || $page=='Liste sujets'){
        echo "
    <nav class='navbar navbar-expand-lg bg-secondary text-uppercase fixed-top' id='mainNav'>
        <div class='container'>
            <a class='navbar-brand' href='#page-top'>Haut de page</a>
            <button class='navbar-toggler text-uppercase font-weight-bold bg-primary text-white rounded' type='button' data-bs-toggle='collapse' data-bs-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'>
                Menu
                <i class='fas fa-bars'></i>
            </button>
            <div class='collapse navbar-collapse' id='navbarResponsive'>
                <ul class='navbar-nav ms-auto'>";
                    if($page != 'Home'){
                        echo "
                        <li class='nav-item mx-0 mx-lg-1'><a class='nav-link py-3 px-0 px-lg-3 rounded' href='home.php'>Mon compte</a></li>";
                    }
                    if($_SESSION['fonction'] == 'administrateur'){
                        echo "
                        <li class='nav-item mx-0 mx-lg-1'><a class='nav-link py-3 px-0 px-lg-3 rounded' href='tableau_de_bord.php'>Tableau de bord</a></li>";
                    }
                
                    if($_SESSION['fonction'] == 'enseignant'){
                        echo "
                        <li class='nav-item mx-0 mx-lg-1'><a class='nav-link py-3 px-0 px-lg-3 rounded' href='mon_sujet.php'>Mon sujet</a></li>";
                    }
                    echo "
                    <li class='nav-item mx-0 mx-lg-1'><a class='nav-link py-3 px-0 px-lg-3 rounded' href='liste_sujet.php'>Liste sujets</a></li>
                    <li>
                        <form action='home.php' method='post'>
                            <button class='navbar text-uppercase font-weight-bold bg-primary text-white rounded' type='submit' name='deconnexion' value='Deconnexion'>
                                Déconnexion
                            </button>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </nav>";
    echo"
    <noscript>
        <section class='page-section m-2'>
            <h2> Veuillez autorisez javascript pour un meilleur rendu </h2>";
            if($page != 'Home'){
                echo "
                <a class='nav-link py-3 px-0 px-lg-3 rounded' href='home.php'>Mon compte</a>";
            }
            if($_SESSION['fonction'] == 'administrateur'){
                echo "
                <a class='nav-link py-3 px-0 px-lg-3 rounded' href='tableau_de_bord.php'>Tableau de bord</a>";
            }
        
            if($_SESSION['fonction'] == 'enseignant'){
                echo "
                <a class='nav-link py-3 px-0 px-lg-3 rounded' href='mon_sujet.php'>Mon sujet</a>";
            }
            echo"
            <a class='nav-link py-3 px-0 px-lg-3 rounded' href='home.php'>Mon compte</a>
            <form action='home.php' method='post'>
                <button class='navbar text-uppercase font-weight-bold bg-primary text-white rounded' type='submit' name='deconnexion' value='Deconnexion'>
                    Déconnexion
                </button>
            </form>
        </section>
    </noscript>";
      }
      else if ($page == 'Tableau de bord'){
        echo "
    <nav class='navbar navbar-expand-lg bg-secondary text-uppercase fixed-top' id='mainNav'>
        <div class='container'>
            <a class='navbar-brand' href='#page-top'>Haut de page</a>
            <button class='navbar-toggler text-uppercase font-weight-bold bg-primary text-white rounded' type='button' data-bs-toggle='collapse' data-bs-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'>
                Menu
                <i class='fas fa-bars'></i>
            </button>
            <div class='collapse navbar-collapse' id='navbarResponsive'>
                <ul class='navbar-nav ms-auto'>
                    <li class='nav-item mx-0 mx-lg-1'>
                        <a class='nav-link py-3 px-0 px-lg-3 rounded' href='home.php'>Mon compte</a>
                    </li>
                    <li class='nav-item mx-0 mx-lg-1'>
                        <a class='nav-link py-3 px-0 px-lg-3 rounded' href='tableau_de_bord.php'>Tableau de bord</a>
                    </li>
                    <li class='nav-item mx-0 mx-lg-1'>
                        <a class='nav-link py-3 px-0 px-lg-3 rounded' href='liste_sujet.php'>Liste sujets</a>
                    </li>
                    <li>
                        <form action='tableau_de_bord.php' method='post'>
                            <button class='navbar text-uppercase font-weight-bold bg-primary text-white rounded' type='submit' name='deconnexion' value='Deconnexion'>
                                Déconnexion
                            </button>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </nav>";
    echo "
    <noscript>
        <section class='page-section m-2'>
            <h2> Veuillez autorisez javascript pour un meilleur rendu </h2>
            <a class='nav-link py-3 px-0 px-lg-3 rounded' href='https://moduleweb.esigelec.fr/grp_9_5/index.php'>Accueil</a>
            <a class='nav-link py-3 px-0 px-lg-3 rounded' href='home.php'>Mon compte</a>
            <a class='nav-link py-3 px-0 px-lg-3 rounded' href='tableau_de_bord.php'>Tableau de bord</a>
            <a class='nav-link py-3 px-0 px-lg-3 rounded' href='liste_sujet.php'>Liste sujets</a>
            <form action='tableau_de_bord.php' method='post'>
                <button class='navbar text-uppercase font-weight-bold bg-primary text-white rounded' type='submit' name='deconnexion' value='Deconnexion'>
                    Déconnexion
                </button>
            </form>
        </section>
    </noscript>";
      }
      else if ($page == 'Mon sujet'){
        echo "
    <nav class='navbar navbar-expand-lg bg-secondary text-uppercase fixed-top' id='mainNav'>
        <div class='container'>
            <a class='navbar-brand' href='#page-top'>Haut de page</a>
            <button class='navbar-toggler text-uppercase font-weight-bold bg-primary text-white rounded' type='button' data-bs-toggle='collapse' data-bs-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'>
                Menu
                <i class='fas fa-bars'></i>
            </button>
            <div class='collapse navbar-collapse' id='navbarResponsive'>
                <ul class='navbar-nav ms-auto'>
                    <li class='nav-item mx-0 mx-lg-1'><a class='nav-link py-3 px-0 px-lg-3 rounded' href='home.php'>Mon compte</a></li>
                    <li class='nav-item mx-0 mx-lg-1'><a class='nav-link py-3 px-0 px-lg-3 rounded' href='liste_sujet.php'>Liste sujets</a></li>
                    <li>
                        <form action='tableau_de_bord.php' method='post'>
                            <button class='navbar text-uppercase font-weight-bold bg-primary text-white rounded' type='submit' name='deconnexion' value='Deconnexion'>
                                Déconnexion
                            </button>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </nav>";
    echo "
    <noscript>
        <section class='page-section m-2'>
            <h2> Veuillez autorisez javascript pour un meilleur rendu </h2>
            <a class='nav-link py-3 px-0 px-lg-3 rounded' href='home.php'>Mon compte</a>
            <a class='nav-link py-3 px-0 px-lg-3 rounded' href='liste_sujet.php'>Liste sujets</a>
            <form action='tableau_de_bord.php' method='post'>
                <button class='navbar text-uppercase font-weight-bold bg-primary text-white rounded' type='submit' name='deconnexion' value='Deconnexion'>
                    Déconnexion
                </button>
            </form>
        </section>
    </noscript>";
      }else if ($page == 'Sujet détaillé'){
        echo "
    <nav class='navbar navbar-expand-lg bg-secondary text-uppercase fixed-top' id='mainNav'>
        <div class='container'>
            <a class='navbar-brand' href='#page-top'>Haut de page</a>
            <button class='navbar-toggler text-uppercase font-weight-bold bg-primary text-white rounded' type='button' data-bs-toggle='collapse' data-bs-target='#navbarResponsive' aria-controls='navbarResponsive' aria-expanded='false' aria-label='Toggle navigation'>
                Menu
                <i class='fas fa-bars'></i>
            </button>
            <div class='collapse navbar-collapse' id='navbarResponsive'>
                <ul class='navbar-nav ms-auto'>
                    <li class='nav-item mx-0 mx-lg-1'><a class='nav-link py-3 px-0 px-lg-3 rounded' href='home.php'>Mon compte</a></li>";
                    if($_SESSION['fonction'] == 'enseignant'){
                        echo "
                    <li class='nav-item mx-0 mx-lg-1'><a class='nav-link py-3 px-0 px-lg-3 rounded' href='mon_sujet.php'>Mon sujet</a></li>";
                    }
                    if($_SESSION['fonction'] == 'administrateur'){
                        echo "
                    <li class='nav-item mx-0 mx-lg-1'><a class='nav-link py-3 px-0 px-lg-3 rounded' href='tableau_de_bord.php'>Tableau de bord</a></li>";
                    }
                    echo"
                    <li class='nav-item mx-0 mx-lg-1'><a class='nav-link py-3 px-0 px-lg-3 rounded' href='liste_sujet.php'>Liste sujets</a></li>
                    <li>
                        <form action='tableau_de_bord.php' method='post'>
                            <button class='navbar text-uppercase font-weight-bold bg-primary text-white rounded' type='submit' name='deconnexion' value='Deconnexion'>
                                Déconnexion
                            </button>
                        </form>
                    </li>
                </ul>
            </div>
        </div>
    </nav>";
    echo "
    <noscript>
        <section class='page-section m-2'>
            <h2> Veuillez autorisez javascript pour un meilleur rendu </h2>";
            if($_SESSION['fonction'] == 'enseignant'){
                echo "
                <a class='nav-link py-3 px-0 px-lg-3 rounded' href='mon_sujet.php'>Mon sujet</a>";
            }
            if($_SESSION['fonction'] == 'administrateur'){
                echo "
                <a class='nav-link py-3 px-0 px-lg-3 rounded' href='tableau_de_bord.php'>Tableau de bord</a>";
            }
            echo"<a class='nav-link py-3 px-0 px-lg-3 rounded' href='home.php'>Mon compte</a>
            <a class='nav-link py-3 px-0 px-lg-3 rounded' href='liste_sujet.php'>Liste sujets</a>
            <form action='tableau_de_bord.php' method='post'>
            <button class='navbar text-uppercase font-weight-bold bg-primary text-white rounded' type='submit' name='deconnexion' value='Deconnexion'>
                Déconnexion
            </button>
        </form>
        </section>
    </noscript>";
      }
    }


    function headerBig($title){
        echo "
    <header class='masthead bg-primary text-white text-center'>
        <div class='container d-flex align-items-center flex-column'>
            <h1 class='masthead-heading text-uppercase mb-0'>$title</h1>
        </div>
    </header>";
    }
    function deconnexion(){
        session_start();
        session_destroy();
        header('Location: https://moduleweb.esigelec.fr/grp_9_5/index.php');
        exit;
    }

?>
