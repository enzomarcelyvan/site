Site développé par Enzo Marcel Yvan

Pour démarrer le site:
1. Partie SQL
  Importer la base de données via le fichier 'bdd.sql'
  Importer les tuples de test via le fichier 'insert.sql'

2. (optionnel)Partie serveur
  Télécharger tous les fichier sur votre serveur FTP

3. Utilisation du site

  Première page Accueil du site avec l'affichage des sujets en fonction des dominantes
    > Lien Connexion
      Redirige vers la page de connexion
        Vous pouvez vous connecter en tant que Etudiant/Administrateur/Enseignant :
        Etudiant : identifiant = etud, mot de passe = etud;
                   identifiant = etud2, mot de passe = etud2;

        Administrateur : identifiant = admin, mot de passe = admin;

        Enseignant : identifiant = enseignant, mot de passe = enseignant;
                     identifiant = enseignantSansSujet, mot de passe = enseignant;
                     identifiant = enseignant2, mot de passe = enseignant2;
                     
    > Lien Inscription
      Redirige vers la page d'inscription
        Vous pouvez vous inscrire en tant qu'élève.

