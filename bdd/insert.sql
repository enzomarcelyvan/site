/*Insertion pour la table departement*/
INSERT INTO `bdd_9_5`.`Departement` (`id_departement`, `nom_departement`) VALUES (NULL, 'TIC');
INSERT INTO `bdd_9_5`.`Departement` (`id_departement`, `nom_departement`) VALUES (NULL, 'SEI');
INSERT INTO `bdd_9_5`.`Departement` (`id_departement`, `nom_departement`) VALUES (NULL, 'GEE');
INSERT INTO `bdd_9_5`.`Departement` (`id_departement`, `nom_departement`) VALUES (NULL, 'ET');
/*Insertion pour la table Dominante*/
INSERT INTO `bdd_9_5`.`Dominante` (`id_Dominante`, `nom_Dominante`, `departement`) VALUES (NULL, 'CERT', '1');
INSERT INTO `bdd_9_5`.`Dominante` (`id_Dominante`, `nom_Dominante`, `departement`) VALUES (NULL, 'BDTN', '1');
INSERT INTO `bdd_9_5`.`Dominante` (`id_Dominante`, `nom_Dominante`, `departement`) VALUES (NULL, 'IA-IR', '1');
INSERT INTO `bdd_9_5`.`Dominante` (`id_Dominante`, `nom_Dominante`, `departement`) VALUES (NULL, 'ISN', '1');
INSERT INTO `bdd_9_5`.`Dominante` (`id_Dominante`, `nom_Dominante`, `departement`) VALUES (NULL, 'IF', '1');
INSERT INTO `bdd_9_5`.`Dominante` (`id_Dominante`, `nom_Dominante`, `departement`) VALUES (NULL, 'ARI', '3');
INSERT INTO `bdd_9_5`.`Dominante` (`id_Dominante`, `nom_Dominante`, `departement`) VALUES (NULL, 'EDD', '3');
INSERT INTO `bdd_9_5`.`Dominante` (`id_Dominante`, `nom_Dominante`, `departement`) VALUES (NULL, 'GET', '3');
INSERT INTO `bdd_9_5`.`Dominante` (`id_Dominante`, `nom_Dominante`, `departement`) VALUES (NULL, 'IA-DES', '3');
INSERT INTO `bdd_9_5`.`Dominante` (`id_Dominante`, `nom_Dominante`, `departement`) VALUES (NULL, 'MCTGE', '2');
INSERT INTO `bdd_9_5`.`Dominante` (`id_Dominante`, `nom_Dominante`, `departement`) VALUES (NULL, 'ISE-VA', '2');
INSERT INTO `bdd_9_5`.`Dominante` (`id_Dominante`, `nom_Dominante`, `departement`) VALUES (NULL, 'ISE-OC', '2');
INSERT INTO `bdd_9_5`.`Dominante` (`id_Dominante`, `nom_Dominante`, `departement`) VALUES (NULL, 'ISYMED', '2');
INSERT INTO `bdd_9_5`.`Dominante` (`id_Dominante`, `nom_Dominante`, `departement`) VALUES (NULL, 'ESAA', '4');
INSERT INTO `bdd_9_5`.`Dominante` (`id_Dominante`, `nom_Dominante`, `departement`) VALUES (NULL, 'ICOM', '4');

/* Insertion pour la table sujet */
INSERT INTO `bdd_9_5`.`Sujet` (`id_sujet`, `departement`, `Dominante`, `nom_sujet`, `resume_sujet`, `nb_places_rest`, `chemin_piece_jointe`) VALUES (NULL, '1', '4', 'Création d''un site web', 'Mise en place d''un site web en groupe de 4 pour une entreprise commercial.', '30', 'sujet1.pdf');
INSERT INTO `bdd_9_5`.`Sujet` (`id_sujet`, `departement`, `Dominante`, `nom_sujet`, `resume_sujet`, `nb_places_rest`, `chemin_piece_jointe`) VALUES (NULL, '2', '13', 'Fauteuil autonome', 'Ce sujet porte sur la réalisation d''un fauteuil autonome. Le matériel nécessaire et le nombres d''équipe sera determiné plus tard.', '0', NULL);

/*Insertion pour la table Personne*/
INSERT INTO `bdd_9_5`.`Personne` (`id_Personne`, `nom`, `prenom`, `date_naissance`, `identifiant`, `mot_de_passe`, `fonction`) VALUES (NULL, 'Placide', 'Enzo', '2021-11-02', 'admin', '$2y$10$vN9iB1yXityhI4xX6mtSBOWI5f3r1O.Km20obp2pTfV4LH25YVfSi', 'administrateur');
/*Etudiants*/
INSERT INTO `bdd_9_5`.`Personne` (`id_Personne`, `nom`, `prenom`, `date_naissance`, `identifiant`, `mot_de_passe`, `fonction`) VALUES (NULL, 'Biondokin Mbassi', 'Yvan', '2000-02-18', 'etud', '$2y$10$5M1f0bPg4Ar17yOMLwA5q.HpUll3ogmi3ArWGrsQ.uBbWVF.cLLI6', 'etudiant');
INSERT INTO `bdd_9_5`.`Etudiant` (`id_etudiant`, `sujet`, `departement`) VALUES ('2', NULL, '1');
INSERT INTO `bdd_9_5`.`Personne` (`id_Personne`, `nom`, `prenom`, `date_naissance`, `identifiant`, `mot_de_passe`, `fonction`) VALUES (NULL, 'Jean', 'Test', '2000-02-18', 'etud2', '$2y$10$fxp0EbTvH/v87eow/r4iWeJMdDOtywr9VdKFkEw6h9xhbnzjZBnP2', 'etudiant');
INSERT INTO `bdd_9_5`.`Etudiant` (`id_etudiant`, `sujet`, `departement`) VALUES ('3', NULL, '2');
/*Enseignants*/
INSERT INTO `bdd_9_5`.`Personne` (`id_Personne`, `nom`, `prenom`, `date_naissance`, `identifiant`, `mot_de_passe`, `fonction`) VALUES (NULL, 'Songo Tiwa', 'Marcel', '2021-11-22', 'enseignant', '$2y$10$zfjxxP51F.byROoCwXL5u.GfKPtAiRhHmfe/vfRdreECZZai.K8pW', 'enseignant');
INSERT INTO `bdd_9_5`.`Enseignant` (`id_enseignant`, `mon_sujet`) VALUES ('3', '1');
INSERT INTO `bdd_9_5`.`Personne` (`id_Personne`, `nom`, `prenom`, `date_naissance`, `identifiant`, `mot_de_passe`, `fonction`) VALUES (NULL, 'Jean', 'Verre', '2021-11-22', 'enseignant2', '$2y$10$D38VxrqlCF5yOdTB/xEwi.ON20yk2U.FcBl6PgLeht7iiqUS4fDhG', 'enseignant');
INSERT INTO `bdd_9_5`.`Enseignant` (`id_enseignant`, `mon_sujet`) VALUES ('4', '2');
INSERT INTO `bdd_9_5`.`Personne` (`id_Personne`, `nom`, `prenom`, `date_naissance`, `identifiant`, `mot_de_passe`, `fonction`) VALUES (NULL, 'Jean', 'Prof', '2021-11-22', 'enseignantSansSujet', '$2y$10$zfjxxP51F.byROoCwXL5u.GfKPtAiRhHmfe/vfRdreECZZai.K8pW', 'enseignant');
INSERT INTO `bdd_9_5`.`Enseignant` (`id_enseignant`, `mon_sujet`) VALUES ('5', NULL);



