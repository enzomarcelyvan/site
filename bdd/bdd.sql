USE bdd_9_5;

DROP TABLE IF EXISTS Etudiant;
DROP TABLE IF EXISTS Enseignant;
DROP TABLE IF EXISTS Sujet;
DROP TABLE IF EXISTS Dominante;
DROP TABLE IF EXISTS Departement;
DROP TABLE IF EXISTS Personne;

CREATE TABLE IF NOT EXISTS Personne(
	id_personne INT PRIMARY KEY AUTO_INCREMENT,
	nom VARCHAR(30),
	prenom VARCHAR(30),
	date_naissance DATE,
	identifiant VARCHAR(30) UNIQUE,
	mot_de_passe VARCHAR(60),
	fonction VARCHAR(30) CHECK(fonction='etudiant' OR fonction='enseignant' OR fonction='administrateur')
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS Departement(
	id_departement INT PRIMARY KEY AUTO_INCREMENT,
	nom_departement VARCHAR (30) NOT NULL
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS Dominante(
	id_dominante INT PRIMARY KEY AUTO_INCREMENT,
	nom_dominante VARCHAR(20),
	departement INT,
	CONSTRAINT fk_dominante_departement
		FOREIGN KEY (departement)
		REFERENCES Departement(id_departement)
		ON DELETE CASCADE
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS Sujet(
	id_sujet INT PRIMARY KEY AUTO_INCREMENT,
	departement INT,
	dominante INT,
	nom_sujet VARCHAR(30),
	resume_sujet VARCHAR(200),
	nb_places_rest INT,
	chemin_piece_jointe VARCHAR (50),
	CONSTRAINT fk_sujet_departement
		FOREIGN KEY (departement)
		REFERENCES Departement(id_departement)
		ON DELETE SET NULL,
	CONSTRAINT fk_sujet_dominante
		FOREIGN KEY (dominante)
		REFERENCES Dominante(id_dominante)
		ON DELETE SET NULL
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS Enseignant(
	id_enseignant INT PRIMARY KEY AUTO_INCREMENT,
	mon_sujet INT,
	CONSTRAINT fk_enseignant_personne
		FOREIGN KEY (id_enseignant)
		REFERENCES Personne(id_personne)
		ON DELETE CASCADE,
	CONSTRAINT fk_enseignant_sujet
		FOREIGN KEY (mon_sujet)
		REFERENCES Sujet(id_sujet)
		ON DELETE SET NULL
)ENGINE=InnoDB;


CREATE TABLE IF NOT EXISTS Etudiant(
	id_etudiant INT PRIMARY KEY AUTO_INCREMENT,
	sujet INT,
	departement INT,
	CONSTRAINT fk_etudiant_sujet
		FOREIGN KEY (sujet)
		REFERENCES Sujet(id_sujet)
		ON DELETE SET NULL,
	CONSTRAINT fk_etudiant_departement
		FOREIGN KEY (departement)
		REFERENCES Departement(id_departement)
		ON DELETE SET NULL,
	CONSTRAINT fk_etudiant_personne
		FOREIGN KEY (id_etudiant)
		REFERENCES Personne(id_personne)
		ON DELETE CASCADE
)ENGINE=InnoDB;
