/*!
* Start Bootstrap - Freelancer v7.0.4 (https://startbootstrap.com/theme/freelancer)
* Copyright 2013-2021 Start Bootstrap
* Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-freelancer/blob/master/LICENSE)
*/
//
// Scripts
// 

window.addEventListener('DOMContentLoaded', event => {

    // Navbar shrink function
    var navbarShrink = function () {
        const navbarCollapsible = document.body.querySelector('#mainNav');
        if (!navbarCollapsible) {
            return;
        }
        if (window.scrollY === 0) {
            navbarCollapsible.classList.remove('navbar-shrink')
        } else {
            navbarCollapsible.classList.add('navbar-shrink')
        }

    };

    // Shrink the navbar 
    navbarShrink();

    // Shrink the navbar when page is scrolled
    document.addEventListener('scroll', navbarShrink);

    // Activate Bootstrap scrollspy on the main nav element
    const mainNav = document.body.querySelector('#mainNav');
    if (mainNav && document.body.id == "page-top") {
        new bootstrap.ScrollSpy(document.body, {
            target: '#mainNav',
            offset: 72,
        });
    };

    // Collapse responsive navbar when toggler is visible
    const navbarToggler = document.body.querySelector('.navbar-toggler');
    const responsiveNavItems = [].slice.call(
        document.querySelectorAll('#navbarResponsive .nav-link')
    );
    responsiveNavItems.map(function (responsiveNavItem) {
        responsiveNavItem.addEventListener('click', () => {
            if (window.getComputedStyle(navbarToggler).display !== 'none') {
                navbarToggler.click();
            }
        });
    });

});

// Fonction pour le tableau de bord
//Personne
function modifierPersonne(id_personne){
    window.location.replace("tableau_de_bord.php?menu=personnes&modif="+id_personne);
}
function supprimerPersonne(id_personne){
    var answer = window.confirm("Voulez-vous vraiment supprimer la personne qui a pour id :"+id_personne+" ?");

	if(answer){
		window.location.replace("tableau_de_bord.php?menu=personnes&supprimer="+id_personne);
	}
}
//Sujet
function modifierSujet(id_sujet){
    window.location.replace("tableau_de_bord.php?menu=sujets&modif="+id_sujet);
}
function supprimerSujet(id_sujet){
    var answer = window.confirm("Voulez-vous vraiment supprimer le sujet qui a pour id :"+id_sujet+" ?");

	if(answer){
		window.location.replace("tableau_de_bord.php?menu=sujets&supprimer="+id_sujet);
	}
}
//Departement
function modifierDepartement(id_departement){
    window.location.replace("tableau_de_bord.php?menu=departements&modif="+id_departement);
}
function supprimerDepartement(id_departement){
    var answer = window.confirm("Voulez-vous vraiment supprimer le deparement qui a pour id :"+id_departement+" ?");

	if(answer){
		window.location.replace("tableau_de_bord.php?menu=departements&supprimer="+id_departement);
	}
}
//Dominante
function modifierDominante(id_dominante){
    window.location.replace("tableau_de_bord.php?menu=dominante&modif="+id_dominante);
}
function supprimerDominante(id_dominante){
    var answer = window.confirm("Voulez-vous vraiment supprimer le dominante qui a pour id :"+id_dominante+" ?");

	if(answer){
		window.location.replace("tableau_de_bord.php?menu=dominante&supprimer="+id_dominante);
	}
}
//reinit
function reinitialiserTable(id_table){
    var tables = ["Sujet",
    "Personne",
    "Departement",
    "Dominante"];
    var answer = window.confirm("Voulez-vous vraiment réinitialiser la table :"+tables[id_table]+" ? (Attention action irréversible)");

	if(answer){
		window.location.replace("tableau_de_bord.php?menu=reinit&supprimer="+tables[id_table]);
	}
}