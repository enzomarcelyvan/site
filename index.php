<?php
include('scripts/function.php');
include('scripts/db.php');
$db = initDb();
enTete("Accueil");
echo "<body id='page-top'>
<!-- Navigation-->";
nav("Accueil");
    
echo"<!-- Masthead-->
<header class='bg-primary text-white text-center' style='padding: 0px 0px;margin: 0px 0px;'>
    <div id='monCarousel' class='container d-flex align-items-center carousel slide' data-bs-ride='carousel'
     style='margin: 0px 0px; padding: 0px 0px; min-width: 100%; max-width: 100%'>
        
        <div class='carousel-inner' role='listbox'>
            <div class='carousel-item active'>
                <img src='dist/assets/img/accueil.jpg' style='min-width: 100%;min-height: 100%;max-height: 100%; max-width: 100%' alt='image de présentation'>
                <div class='carousel-caption'>
                    <!-- Masthead Heading-->
                    <h1 class='masthead-heading text-uppercase mb-0'>Bienvenue</h1>
                    <!-- Icon Divider-->
                    <div class='divider-custom divider-light'>
                        <div class='divider-custom-line'></div>
                        <div class='divider-custom-icon'><i class='fas fa-star'></i></div>
                        <div class='divider-custom-line'></div>
                    </div>
                    <!-- Masthead Subheading-->
                    <p class='masthead-subheading font-weight-light mb-0'>Site référant les sujets de projets ingénieur</p>
                </div>
            </div>
            <div class='carousel-item'>
                <img src='dist/assets/img/charliat.jpg' style='min-width: 100%;min-height: 100%;' alt='image de présentation'>
                <div class='carousel-caption'>
                    <!-- Masthead Heading-->
                    <h1 class='masthead-heading text-uppercase mb-0'>Bienvenue</h1>
                    <!-- Icon Divider-->
                    <div class='divider-custom divider-light'>
                        <div class='divider-custom-line'></div>
                        <div class='divider-custom-icon'><i class='fas fa-star'></i></div>
                        <div class='divider-custom-line'></div>
                    </div>
                    <!-- Masthead Subheading-->
                    <p class='masthead-subheading font-weight-light mb-0'>Site référant les sujets de projets ingénieur</p>
                </div>
            </div>
            <div class='carousel-item'>
                <img src='dist/assets/img/cour.jpg' style='min-width: 100%;min-height: 100%;' alt='image de présentation'>
                <div class='carousel-caption'>
                    <!-- Masthead Heading-->
                    <h1 class='masthead-heading text-uppercase mb-0'>Bienvenue</h1>
                    <!-- Icon Divider-->
                    <div class='divider-custom divider-light'>
                        <div class='divider-custom-line'></div>
                        <div class='divider-custom-icon'><i class='fas fa-star'></i></div>
                        <div class='divider-custom-line'></div>
                    </div>
                    <!-- Masthead Subheading-->
                    <p class='masthead-subheading font-weight-light mb-0'>Site référant les sujets de projets ingénieur</p>
                </div>
            </div>
            <div class='carousel-item'>
                <img src='dist/assets/img/irseem.jpg' style='min-width: 100%;min-height: 100%;' alt='image de présentation'>
                <div class='carousel-caption'>
                    <!-- Masthead Heading-->
                    <h1 class='masthead-heading text-uppercase mb-0'>Bienvenue</h1>
                    <!-- Icon Divider-->
                    <div class='divider-custom divider-light'>
                        <div class='divider-custom-line'></div>
                        <div class='divider-custom-icon'><i class='fas fa-star'></i></div>
                        <div class='divider-custom-line'></div>
                    </div>
                    <!-- Masthead Subheading-->
                    <p class='masthead-subheading font-weight-light mb-0'>Site référant les sujets de projets ingénieur</p>
                </div>
            </div>
        </div>
        <button class='carousel-control-prev' type='button' data-bs-target='#monCarousel' data-bs-slide='prev'>
            <span class='carousel-control-prev-icon' aria-hidden='true'></span>
            <span class='visually-hidden'>Previous</span>
        </button>
        <button class='carousel-control-next' type='button' data-bs-target='#monCarousel' data-bs-slide='next'>
            <span class='carousel-control-next-icon' aria-hidden='true'></span>
            <span class='visually-hidden'>Next</span>
        </button>
    </div>
</header>

<!-- listeSujets Section-->
<section class='page-section listeSujets' id='listeSujets'>
    <div class='container'>
        <!-- listeSujets Section Heading-->
        <h2 class='page-section-heading text-center text-uppercase text-secondary mb-0'>Les sujets</h2>
        <!-- Icon Divider-->
        <div class='divider-custom'>
            <div class='divider-custom-line'></div>
            <div class='divider-custom-icon'><i class='fas fa-star'></i></div>
            <div class='divider-custom-line'></div>
        </div>
        <!-- listeSujets Grid Items-->
        <div class='row justify-content-center'>";
        $cpt = 1;
        $dominantes = getAllDominantes($db);
        foreach($dominantes as $dominante){
            echo"
            <!-- listeSujets Item $cpt-->
            <div class='col-md-6 col-lg-4 mb-5 mt-5'>
                <div class='listeSujets-item mx-auto' data-bs-toggle='modal' data-bs-target='#listeSujetsModal$cpt'>
                    <div class='listeSujets-item-caption d-flex align-items-center justify-content-center h-100 w-100'>
                        <div class='listeSujets-item-caption-content text-center text-white'><i class='fas fa-plus fa-3x'></i></div>
                    </div>
                    <img class='img-fluid' src='dist/assets/img/listeSujets/".$dominante['nom_dominante'].".jpg' alt='dominante ".$dominante['nom_dominante']."' />
                    <h2>Dominante ".$dominante['nom_dominante']."</h2>
                </div>
            </div>";
            $cpt++;
        }
            
            
    echo"
        </div>
    </div>
</section>
<!-- About Section-->
<section class='page-section bg-primary text-white mb-0' id='aPropos'>
    <div class='container'>
        <!-- About Section Heading-->
        <h2 class='page-section-heading text-center text-uppercase text-white'>A propos</h2>
        <!-- Icon Divider-->
        <div class='divider-custom divider-light'>
            <div class='divider-custom-line'></div>
            <div class='divider-custom-icon'><i class='fas fa-star'></i></div>
            <div class='divider-custom-line'></div>
        </div>
        <!-- About Section Content-->
        <div class='row'>
            <div class='col-lg-4 ms-auto'>
                <p class='lead'>Ce site regroupe tous les sujets de projets ingénieur en fonction de leur dominantes. Les enseignants présentent leur sujet ici.</p>
            </div>
            <div class='col-lg-4 me-auto'>
                <p class='lead'>Les étudiants peuvent s'inscrire à un sujet, attention il y a un nombre de places limitées par sujet.</p>
            </div>
        </div>
    </div>
</section>
<!-- listeSujets Modals-->";
$cpt = 1;
$dominantes = getAllDominantes($db);
foreach($dominantes as $dominante){
    echo"<!-- listeSujets Modal $cpt-->
    <div class='listeSujets-modal modal fade' id='listeSujetsModal$cpt' tabindex='-1' aria-labelledby='listeSujetsModal$cpt' aria-hidden='true'>
        <div class='modal-dialog modal-xl'>
            <div class='modal-content'>
                <div class='modal-header border-0'><button class='btn-close' type='button' data-bs-dismiss='modal' aria-label='Close'></button></div>
                <div class='modal-body text-center pb-5'>
                    <div class='container'>
                        <div class='row justify-content-center'>
                            <div class='col-lg-8'>
                                <!-- listeSujets Modal - Title-->
                                <h2 class='listeSujets-modal-title text-secondary text-uppercase mb-0'>Sujets - Dominante ".$dominante['nom_dominante']."</h2>
                                <!-- Icon Divider-->
                                <div class='divider-custom'>
                                    <div class='divider-custom-line'></div>
                                    <div class='divider-custom-icon'><i class='fas fa-star'></i></div>
                                    <div class='divider-custom-line'></div>
                                </div>
                                <!-- listeSujets Modal - Image-->
                                <img class='img-fluid rounded mb-5' src='dist/assets/img/listeSujets/".$dominante['nom_dominante'].".jpg' alt='dominante ".$dominante['nom_dominante']."' />
                                <!-- listeSujets Modal - Liste-->
                                <div class='table-responsive'>
                                    <table class='table table-striped table-bordered table-hover'>
                                        <caption>Liste des sujets</caption>
                                        <thead>
                                        <tr>
                                            <th scope='col'>Département rattaché</th>
                                            <th scope='col'>Nom sujet</th>
                                            <th scope='col'>Résumé</th>
                                        </tr>
                                        </thead>
                                        <tbody>";
                                        $sujets = getAllSujetsByDominante($db,$dominante['id_dominante']);
                                        foreach($sujets as $sujet){
                                            echo"<tr>
                                                <td>" . getDepartement($db,$sujet['departement'])['nom_departement'] . "</td>
                                                <td>" . $sujet['nom_sujet'] . "</td>
                                                <td>" . $sujet['resume_sujet'] . "</td>
                                            </tr>";
                                        }
                                        echo"</tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>";
    $cpt++;
}

echo "
<noscript>
    <section class='page-section m-2'>
        <h2> Veuillez autorisez javascript pour un meilleur rendu </h2>";
        echo"<div class='container'>";
        $cpt = 1;
        $dominantes = getAllDominantes($db);
        foreach($dominantes as $dominante){
            echo"
    <div class='row justify-content-center'>
        <div class='col-lg-8'>
            <!-- listeSujets Modal - Title-->
            <h2 class='listeSujets-modal-title text-secondary text-uppercase mb-0'>Sujets - Dominante ".$dominante['nom_dominante']."</h2>
            <!-- Icon Divider-->
            <div class='divider-custom'>
                <div class='divider-custom-line'></div>
                <div class='divider-custom-icon'><i class='fas fa-star'></i></div>
                <div class='divider-custom-line'></div>
            </div>
            <!-- listeSujets Modal - Image-->
            <img class='img-fluid rounded mb-5' src='dist/assets/img/listeSujets/".$dominante['nom_dominante'].".jpg' alt='dominante ".$dominante['nom_dominante']."' />
            <!-- listeSujets Modal - Liste-->
            <div class='table-responsive'>
                <table class='table table-striped table-bordered table-hover'>
                    <caption>Liste des sujets</caption>
                    <thead>
                    <tr>
                        <th scope='col'>Département rattaché</th>
                        <th scope='col'>Nom sujet</th>
                        <th scope='col'>Résumé</th>
                    </tr>
                    </thead>
                    <tbody>";
                    $sujets = getAllSujetsByDominante($db,$dominante['id_dominante']);
                    $nbLignes = 1;
                    foreach($sujets as $sujet){
                        echo"<tr>
                            <td>" . getDepartement($db,$sujet['departement'])['nom_departement'] . "</td>
                            <td>" . $sujet['nom_sujet'] . "</td>
                            <td>" . $sujet['resume_sujet'] . "</td>
                        </tr>";
                        $nbLignes++;
                    }
                    echo"</tbody>
                </table>
            </div>
        </div>
    </div>";
}
    echo"
        </div>    
    </section>
</noscript>";
pied(true);
?>